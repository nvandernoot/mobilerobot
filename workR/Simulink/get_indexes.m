%% Initialization
%
% Get the multibody system indexes to copy-paste them
% in other files like 'simu_def.h'
%

clc;
close all;
clear all;

%% Generate mbs_data and mbs_info

prjname = 'MobileRobot';      % project name
[mbs_data, mbs_info] = mbs_load(prjname,'default');

fprintf('\n');

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     Joints     %%%%
%%%%%%%%%%%%%%%%%%%%%%%%

% floating base
id_FJ_T1 = mbs_get_joint_id(mbs_info,'FJ_T1');
id_FJ_T2 = mbs_get_joint_id(mbs_info,'FJ_T2');
id_FJ_R3 = mbs_get_joint_id(mbs_info,'FJ_R3');

fprintf('// floating base indexes\n');
fprintf('#define FJ_T1 %d\n', id_FJ_T1);
fprintf('#define FJ_T2 %d\n', id_FJ_T2);
fprintf('#define FJ_R3 %d\n', id_FJ_R3);
fprintf('\n');

% wheels
id_Right_axle = mbs_get_joint_id(mbs_info,'Right_axle');
id_Left_axle  = mbs_get_joint_id(mbs_info,'Left_axle');

fprintf('// robot joints indexes\n');
fprintf('#define RIGHT_WHEEL_JOINT %d\n', id_Right_axle);
fprintf('#define LEFT_WHEEL_JOINT  %d\n', id_Left_axle);
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     S sensors     %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% id_S_Sensor_Example = mbs_get_S_sensor_id(mbs_info,'S_Sensor_Example');

fprintf('// S sensors\n');
% fprintf('#define S_SENSOR_EXAMPLE %d\n', id_S_Sensor_Example);
fprintf('\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%     F sensors     %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

id_F_Robot_Ext_Force = mbs_get_F_sensor_id(mbs_info,'Robot_Ext_Force');

fprintf('// F sensors\n');
fprintf('#define F_ROBOT_EXT_FORCE %d\n', id_F_Robot_Ext_Force);
fprintf('\n');
