//---------------------------
//
// Nicolas Van der Noot
//
// Interface to handle a controller
//
//---------------------------

#include "controller_interface.h"

/*
 * Interface of the controller: initialization
 */
void controller_init_interface(MBSdataStruct *MBSdata)
{	
	// inputs
	controller_inputs(MBSdata);
	
	// initialization
	controller_init(MBSdata->user_IO->cvs);
}

/*
 * Interface of the controller: loop
 */
void controller_loop_interface(MBSdataStruct *MBSdata)
{
	// inputs
	controller_inputs(MBSdata);
    
	// loop
    controller_loop(MBSdata->user_IO->cvs);
    
    // outputs
    controller_outputs(MBSdata);
}

/*
 * Interface of the controller: closing operations
 */
void controller_close_interface(MBSdataStruct *MBSdata)
{
	
}
