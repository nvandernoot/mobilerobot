//---------------------------
// Nicolas Van der Noot
//
// Creation : 19-Sep-2013
// Last update : 14-Oct-2014
//---------------------------

#include <stdlib.h>

#include "ControllersStruct.h"


// ---- Controlleres initialization ---- //

// ControllerInputsStruc
ControllerInputs * init_ControllerInputs(void)
{
    ControllerInputs *cvs;

    cvs = (ControllerInputs*) malloc(sizeof(ControllerInputs));

    cvs->t = 0.0;

    cvs->r_wheel_vel = 0.0;

    cvs->l_wheel_vel = 0.0;

    return cvs;
}

// ControllerOutputsStruc
ControllerOutputs * init_ControllerOutputs(void)
{
    ControllerOutputs *cvs;

    int i;

    cvs = (ControllerOutputs*) malloc(sizeof(ControllerOutputs));

    for (i=0;i<2;i++)
    {
        cvs->voltage[i] = 0.0;
    }

    return cvs;
}

// VelocityReferencesStruc
VelocityReferences * init_VelocityReferences(void)
{
    VelocityReferences *cvs;

    int i;

    cvs = (VelocityReferences*) malloc(sizeof(VelocityReferences));

    cvs->r_wheel = 0.0;

    cvs->l_wheel = 0.0;

    for (i=0;i<2;i++)
    {
        cvs->keyboard_refs[i] = 0;
    }

    return cvs;
}

// ControllerStructStruc
ControllerStruct * init_ControllerStruct(void)
{
    ControllerStruct *cvs;

    int i;

    cvs = (ControllerStruct*) malloc(sizeof(ControllerStruct));

    cvs->inputs = init_ControllerInputs();

    cvs->outputs = init_ControllerOutputs();

    cvs->vel_ref = init_VelocityReferences();

    for (i=0;i<2;i++)
    {
        cvs->motor_commands[i] = 0.0;
    }

    return cvs;
}

// ---- Controllers: free ---- //

// ControllerInputs
void free_ControllerInputs(ControllerInputs *cvs)
{
    free(cvs);
}

// ControllerOutputs
void free_ControllerOutputs(ControllerOutputs *cvs)
{
    free(cvs);
}

// VelocityReferences
void free_VelocityReferences(VelocityReferences *cvs)
{
    free(cvs);
}

// ControllerStruct
void free_ControllerStruct(ControllerStruct *cvs)
{
    free_ControllerInputs(cvs->inputs);

    free_ControllerOutputs(cvs->outputs);

    free_VelocityReferences(cvs->vel_ref);

    free(cvs);
}

