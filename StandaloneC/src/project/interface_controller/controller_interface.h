//---------------------------
//
// Nicolas Van der Noot
//
// Interface to handle a controller (header)
//
//---------------------------

#ifndef _CONTROLLER_INTERFACE_H_
#define _CONTROLLER_INTERFACE_H_

#include "simu_def.h"

void controller_init(ControllerStruct *cvs);
void controller_loop(ControllerStruct *cvs);

void controller_inputs(MBSdataStruct *MBSdata);
void controller_outputs(MBSdataStruct *MBSdata);

#endif
