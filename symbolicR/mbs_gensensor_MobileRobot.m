%
%-------------------------------------------------------------
%
%	ROBOTRAN - Version 6.6 (build : february 22, 2008)
%
%	Copyright 
%	Universite catholique de Louvain 
%	Departement de Mecanique 
%	Unite de Production Mecanique et Machines 
%	2, Place du Levant 
%	1348 Louvain-la-Neuve 
%	http://www.robotran.be// 
%
%	==> Generation Date : Thu Jul 10 18:05:24 2014
%
%	==> Project name : MobileRobot
%	==> using XML input file 
%
%	==> Number of joints : 5
%
%	==> Function : F 6 : Sensors Kinematical Informations (sens) 
%	==> Flops complexity : 74
%
%	==> Generation Time :  0.000 seconds
%	==> Post-Processing :  0.000 seconds
%
%-------------------------------------------------------------
%
function [sens] = gensensor(s,tsim,usrfun,isens)

 sens.P = zeros(3,1);
 sens.R = zeros(3,3);
 sens.V = zeros(3,1);
 sens.OM = zeros(3,1);
 sens.A = zeros(3,1);
 sens.OMP = zeros(3,1);
 sens.J = zeros(6,5);

q = s.q; 
qd = s.qd; 
qdd = s.qdd; 
frc = s.frc; 
trq = s.trq; 

% === begin imp_aux === 

% === end imp_aux === 

% ===== BEGIN task 0 ===== 
 
% Sensor Kinematics 



% = = Block_0_0_0_0_0_1 = = 
 
% Trigonometric Variables  

  C3 = cos(q(3));
  S3 = sin(q(3));

% = = Block_0_0_0_0_0_2 = = 
 
% Trigonometric Variables  

  C4 = cos(q(4));
  S4 = sin(q(4));

% = = Block_0_0_0_0_0_3 = = 
 
% Trigonometric Variables  

  C5 = cos(q(5));
  S5 = sin(q(5));

% ====== END Task 0 ====== 

% ===== BEGIN task 1 ===== 
 
switch isens

 
% 
case 1, 


% = = Block_1_0_0_1_1_0 = = 
 
% Symbolic Outputs  

    sens.P(1) = q(1);
    sens.P(3) = s.dpt(3,1);
    sens.R(1,1) = (1.0);
    sens.R(2,2) = (1.0);
    sens.R(3,3) = (1.0);
    sens.V(1) = qd(1);
    sens.A(1) = qdd(1);
 
% 
case 2, 


% = = Block_1_0_0_2_1_0 = = 
 
% Symbolic Outputs  

    sens.P(1) = q(1);
    sens.P(2) = q(2);
    sens.P(3) = s.dpt(3,1);
    sens.R(1,1) = (1.0);
    sens.R(2,2) = (1.0);
    sens.R(3,3) = (1.0);
    sens.V(1) = qd(1);
    sens.V(2) = qd(2);
    sens.A(1) = qdd(1);
    sens.A(2) = qdd(2);
 
% 
case 3, 


% = = Block_1_0_0_3_1_0 = = 
 
% Symbolic Outputs  

    sens.P(1) = q(1);
    sens.P(2) = q(2);
    sens.P(3) = s.dpt(3,1);
    sens.R(1,1) = C3;
    sens.R(1,2) = S3;
    sens.R(2,1) = -S3;
    sens.R(2,2) = C3;
    sens.R(3,3) = (1.0);
    sens.V(1) = qd(1);
    sens.V(2) = qd(2);
    sens.OM(3) = qd(3);
    sens.A(1) = qdd(1);
    sens.A(2) = qdd(2);
    sens.OMP(3) = qdd(3);
 
% 
case 4, 


% = = Block_1_0_0_4_0_2 = = 
 
% Sensor Kinematics 


    ROcp3_14 = C3*C4;
    ROcp3_24 = S3*C4;
    ROcp3_74 = C3*S4;
    ROcp3_84 = S3*S4;
    RLcp3_14 = -s.dpt(2,2)*S3;
    RLcp3_24 = s.dpt(2,2)*C3;
    POcp3_14 = RLcp3_14+q(1);
    POcp3_24 = RLcp3_24+q(2);
    OMcp3_14 = -qd(4)*S3;
    OMcp3_24 = qd(4)*C3;
    ORcp3_14 = -RLcp3_24*qd(3);
    ORcp3_24 = RLcp3_14*qd(3);
    VIcp3_14 = ORcp3_14+qd(1);
    VIcp3_24 = ORcp3_24+qd(2);
    OPcp3_14 = -(qdd(4)*S3+qd(3)*qd(4)*C3);
    OPcp3_24 = qdd(4)*C3-qd(3)*qd(4)*S3;
    ACcp3_14 = qdd(1)-ORcp3_24*qd(3)-RLcp3_24*qdd(3);
    ACcp3_24 = qdd(2)+ORcp3_14*qd(3)+RLcp3_14*qdd(3);

% = = Block_1_0_0_4_1_0 = = 
 
% Symbolic Outputs  

    sens.P(1) = POcp3_14;
    sens.P(2) = POcp3_24;
    sens.P(3) = s.dpt(3,1);
    sens.R(1,1) = ROcp3_14;
    sens.R(1,2) = ROcp3_24;
    sens.R(1,3) = -S4;
    sens.R(2,1) = -S3;
    sens.R(2,2) = C3;
    sens.R(3,1) = ROcp3_74;
    sens.R(3,2) = ROcp3_84;
    sens.R(3,3) = C4;
    sens.V(1) = VIcp3_14;
    sens.V(2) = VIcp3_24;
    sens.OM(1) = OMcp3_14;
    sens.OM(2) = OMcp3_24;
    sens.OM(3) = qd(3);
    sens.A(1) = ACcp3_14;
    sens.A(2) = ACcp3_24;
    sens.OMP(1) = OPcp3_14;
    sens.OMP(2) = OPcp3_24;
    sens.OMP(3) = qdd(3);
 
% 
case 5, 


% = = Block_1_0_0_5_0_3 = = 
 
% Sensor Kinematics 


    ROcp4_15 = C3*C5;
    ROcp4_25 = S3*C5;
    ROcp4_75 = C3*S5;
    ROcp4_85 = S3*S5;
    RLcp4_15 = -s.dpt(2,3)*S3;
    RLcp4_25 = s.dpt(2,3)*C3;
    POcp4_15 = RLcp4_15+q(1);
    POcp4_25 = RLcp4_25+q(2);
    OMcp4_15 = -qd(5)*S3;
    OMcp4_25 = qd(5)*C3;
    ORcp4_15 = -RLcp4_25*qd(3);
    ORcp4_25 = RLcp4_15*qd(3);
    VIcp4_15 = ORcp4_15+qd(1);
    VIcp4_25 = ORcp4_25+qd(2);
    OPcp4_15 = -(qdd(5)*S3+qd(3)*qd(5)*C3);
    OPcp4_25 = qdd(5)*C3-qd(3)*qd(5)*S3;
    ACcp4_15 = qdd(1)-ORcp4_25*qd(3)-RLcp4_25*qdd(3);
    ACcp4_25 = qdd(2)+ORcp4_15*qd(3)+RLcp4_15*qdd(3);

% = = Block_1_0_0_5_1_0 = = 
 
% Symbolic Outputs  

    sens.P(1) = POcp4_15;
    sens.P(2) = POcp4_25;
    sens.P(3) = s.dpt(3,1);
    sens.R(1,1) = ROcp4_15;
    sens.R(1,2) = ROcp4_25;
    sens.R(1,3) = -S5;
    sens.R(2,1) = -S3;
    sens.R(2,2) = C3;
    sens.R(3,1) = ROcp4_75;
    sens.R(3,2) = ROcp4_85;
    sens.R(3,3) = C5;
    sens.V(1) = VIcp4_15;
    sens.V(2) = VIcp4_25;
    sens.OM(1) = OMcp4_15;
    sens.OM(2) = OMcp4_25;
    sens.OM(3) = qd(3);
    sens.A(1) = ACcp4_15;
    sens.A(2) = ACcp4_25;
    sens.OMP(1) = OPcp4_15;
    sens.OMP(2) = OPcp4_25;
    sens.OMP(3) = qdd(3);

end


% ====== END Task 1 ====== 

  

