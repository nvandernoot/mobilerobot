%--------------------------------------------------------------------------
%   Universite catholique de Louvain
%   CEREM : Centre for research in mechatronics
%   http://www.robotran.be  
%   Contact : robotran@prm.ucl.ac.be
%   Version : ROBOTRAN $Version$
%
%   MBsysLab main script template:
%      - featuring default options
%      - to be adapted by the user
%
%   Project : MobileRobot
%   Author : Nicolas Van der Noot
%   Date : 06/07/2014 
%--------------------------------------------------------------------------

%% 1. Initialization and Project Loading [mbs_load]
%--------------------------------------------------------------------------
close all; clear variables; clc;    % Cleaning of the Matlab workspace

% Generate mbs_data
mbs_data        = generate_mbs_data();   % coordinate partitioning done in 'generate_mbs_data'
mbs_data_closed = mbs_data;

% Simulation model parameters [s] 
t_start   = 0.0;    % start time  [s]
t_finish  = 60.0;   % finish time [s]
time_step = 1.0e-3; % time step   [s]


%% -- Simulink -- %%

% name of the .mdl file (without .mdl)
model_name = 'dirdyna_MobileRobot';

full_model_name = sprintf('%s.mdl', model_name);

open_system(full_model_name);

% choose your solver (ode4 chosen in this case)
set_param(model_name, 'Solver','ode4',...
         'StartTime', num2str(t_start),...
         'StopTime' , num2str(t_finish),...
         'FixedStep', num2str(time_step));
     
% uncomment the following line if no coordinate partitioning is required
% mbs_data.DonePart=1;

ud =  get_param([model_name '/S-Function_dirdynared'], 'UserData');
ud.mbs_data = mbs_data;
set_param([model_name '/S-Function_dirdynared'], 'UserData', ud);


%% 2. Direct dynamics
%--------------------------------------------------------------------------

tic
sim(full_model_name); % launch simulation
toc

%% 3. Results analysis
%--------------------------------------------------------------------------

% get back output signals from simulink
q    = simout_q.signals.values;  % position [m] or [rad]
qd   = simout_qd.signals.values; % velocities [m/s] or [rad/s]
t    = tsim.signals.values;      % time [s]
out  = out.signals.values;       % outputs configured in 'simulink_outputs.c'

% wheel velocity references [rad/s]
vel_ref_r = out(:,1);
vel_ref_l = out(:,2);


% Graphical Results: Motors
figure;
subplot(2,1,1);
hold on;

% wheel velocities references [rad/s]
plot(t,vel_ref_r,'b'); % right wheel
plot(t,vel_ref_l,'r'); % left wheel

% wheel real speed [rad/s]
plot(t,qd(:,4),'Color',[33 75 31]./255);   % right wheel
plot(t,qd(:,5),'Color',[131 15 246]./255); % left wheel

xlabel('time [s]');
ylabel('velocity [rad/s]');
title('Wheels velocities');
legend('ref right','ref left','real right','real left');

% Graphical Results: robot position
subplot(2,1,2);
hold on;
plot(q(:,1),q(:,2),'black');                                    
xlabel('x position [m]');
ylabel('y position [m]');
title('Robot position');
