function [] = user_initialization(mbs_data)
% --------------------------
% UCL-CEREM-MBS
%
% Creation : 2014
% Last update : 04/07/2014
%
% -------------------------
% Function called at the beginning of the simulation
% User can define its own commands to be executed before
% running the simulation main loop.

global MBS_user;

% voltages of the two motors (1:right, 2:left)
%    -> structure initialization   
MBS_user.voltage = zeros(1,2);

% obtstacles (x, y, R)
%    -> circle obstcles with a radius R [m], whose centers are
%       located in (x ; y) [m]
MBS_user.circle_obstacles_list = {

	% totem right
    0.0 , -0.4 , 0.12

    % totem left
    0.0 ,  0.4 , 0.12

    % tree
    0.0 ,  0.0 , 0.02
};


% walls description: 4 walls
%    1: repel for x > ...
%    2: repel for x < ...
%    3: repel for y > ...
%    4: repel for y < ...
%  you must provide the corresponding position for these 4 walls (in [m])
MBS_user.walls_list = {
      1.0;
     -1.0;
      1.5;
     -1.5;
};

end
