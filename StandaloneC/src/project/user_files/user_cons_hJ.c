//--------------------------- 
// UCL-CEREM-MBS 
// 
// @version MBsysLab_s 1.7.a 
// 
// Creation : 2006 
// Last update : 01/10/2008 
//--------------------------- 
 
#include "math.h" 
 
#include "MBSdef.h" 
#include "MBSdataStruct.h" 
#include "simu_def.h" 
 
void user_cons_hJ(double *h, double **Jac, MBSdataStruct *MBSdata, double tsim) 
{

/*-- Begin of user code --*/ 

    // The rolling without slipping equations are computed in this file
    // and in the 'user_cons_jdqd.c' file. 

    double theta, c_theta, s_theta;
    
    theta   = MBSdata->q[FJ_R3];
    c_theta = cos(theta);
    s_theta = sin(theta);

    // computation of h and of the Jacobian
    h[1] = 0.0;
    Jac[1][FJ_T1] = -s_theta;
    Jac[1][FJ_T2] = c_theta;
    Jac[1][FJ_R3] = 0.0;
    Jac[1][LEFT_WHEEL_JOINT]  = 0.0;
    Jac[1][RIGHT_WHEEL_JOINT] = 0.0;
    
    h[2] = 0.0;
    Jac[2][FJ_T1] = c_theta;
    Jac[2][FJ_T2] = s_theta;
    Jac[2][FJ_R3] = -SEMI_AXLE;
    Jac[2][LEFT_WHEEL_JOINT]  = -WHEEL_RADIUS;
    Jac[2][RIGHT_WHEEL_JOINT] = 0.0;
    
    h[3] = 0.0;
    Jac[3][FJ_T1] = c_theta;
    Jac[3][FJ_T2] = s_theta;
    Jac[3][FJ_R3] = SEMI_AXLE;
    Jac[3][LEFT_WHEEL_JOINT]  = 0.0;
    Jac[3][RIGHT_WHEEL_JOINT] = -WHEEL_RADIUS;
 
/*-- End of user code --*/
 
}
