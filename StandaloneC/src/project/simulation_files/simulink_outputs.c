//---------------------------
// Nicolas Van der Noot
//
// Creation : 29/10/2013
// Last update : 29/10/2013
//
// simulation outputs for graphs and debug via simulink interface (useless for the Standalone version)
//
//---------------------------

#include "simu_def.h"

/*
 * Defines outputs for the Simulink environment.
 * These outputs are defined in the 'simu_vars_out' section of the 'simu_variables.m' file.
 */
void simulink_outputs(MBSdataStruct *MBSdata)
{
    // variables declaration
    UserIOStruct       *uvs;
    VelocityReferences *vel_ref;

    // variables initialization
    uvs     = MBSdata->user_IO;
    vel_ref = uvs->cvs->vel_ref;

	// time [s]
	uvs->tsim_out = MBSdata->tsim;
	
    // velocity references [rad/s]
	uvs->out[1] = vel_ref->r_wheel;
    uvs->out[2] = vel_ref->l_wheel;
}
