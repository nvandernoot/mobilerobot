function [Qq] = user_JointForces(mbs_data,tsim);
% --------------------------
% UCL-CEREM-MBS
%
% @version MBsysLab_m 1.7.a
%
% Creation : 2006
% Last update : 30/09/2008
% -------------------------
%
%[Qq] = user_JointForces(mbs_data,tsim);
%
% mbs_data : multibody data structure
% tsim : current time
%
% Qq : joint generalized force/torque (for all joints)
% Qq(i) : joint force/torque in joint (i) along its joint ax
%
% this function may use a global structure called MBS_user

global MBS_user MBS_info

Qq = mbs_data.Qq;

%/*-- Begin of user code --*/

%	DC motors equations (inductance neglected)
%	C_load = rho * ((k phi / R) * u_mot - ((k phi * k phi) / R) * (rho * omega_load))

qd = mbs_data.qd;

% motor parameters
K_PHI_MOT = 0.035;  % torque constant [Nm/A]
R_MOT     = 20;     % motor resistance [ohm]
RHO_MOT   = 20;     % motor reductor [-]
L_MOT     = 3.0e-4; % unused: inductance neglected [H]

% get the independent joints indexes
Right_axle_id = mbs_get_joint_id(MBS_info,'Right_axle');
Left_axle_id  = mbs_get_joint_id(MBS_info,'Left_axle');

% motor equations values
k_phi_r   = K_PHI_MOT / R_MOT;
k_phi_2_r = k_phi_r * K_PHI_MOT;

% motors velocities
omega_mot_Rwheel = qd(Right_axle_id) * RHO_MOT;
omega_mot_Lwheel = qd(Left_axle_id)  * RHO_MOT;

% wheel torques
Qq(Right_axle_id) = RHO_MOT * ( k_phi_r * MBS_user.voltage(1) - k_phi_2_r * omega_mot_Rwheel );
Qq(Left_axle_id)  = RHO_MOT * ( k_phi_r * MBS_user.voltage(2) - k_phi_2_r * omega_mot_Lwheel );

%/*-- End of user code --*/ 

return
