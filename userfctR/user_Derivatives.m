function [uxd] = user_Derivatives(ux,mbs_data,tsim)
% --------------------------
% UCL-CEREM-MBS
%
% @version MBsysLab_m 1.7.a
%
% Creation : 2006
% Last update : 30/09/2008
% -------------------------
%
%[uxd] = user_Derivatives(ux,mbs_data,tsim)
%
% ux : user vector of state variables
% mbs_data : multibody data structure
% tsim : current time
%
% uxd : user vector of state derivatives

global MBS_user MBS_info

uxd = zeros(mbs_data.Nux,1);

%/*-- Begin of user code --*/

global MBS_data; 

% The rolling without slipping equations require to integrate
% the floating base velocities to get their correponding positions.

qd = mbs_data.qd;
ux = mbs_data.ux;

% get the floating base joint indexes
FJ_T1_id = mbs_get_joint_id(MBS_info,'FJ_T1');
FJ_T2_id = mbs_get_joint_id(MBS_info,'FJ_T2');
FJ_R3_id = mbs_get_joint_id(MBS_info,'FJ_R3');

% rolling without slipping: updating dependent variables
MBS_data.q(FJ_T1_id) = ux(1);
MBS_data.q(FJ_T2_id) = ux(2);
MBS_data.q(FJ_R3_id) = ux(3);

% rolling without slipping: define variables to integrate
uxd(1) = qd(FJ_T1_id);
uxd(2) = qd(FJ_T2_id);
uxd(3) = qd(FJ_R3_id);

%/*-- End of user code --*/

return
