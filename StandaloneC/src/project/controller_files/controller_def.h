//---------------------------
// Creation : 29/10/2013
// Last update : 04/07/2014
//
// Controller main header file
//
//--------------------------- 

#ifndef controller_def_h
#define controller_def_h
//--------------------*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "ControllersStruct.h"

/*
 * Uncomment the ' #include "simstruc.h" ' line to have access
 * to the ' printf ' function in the controller files
 * (the results of the printf will be available in the 'Command Window' of Matlab)
 *
 * This line must be commented before transfering the controller to the real robot
 */
// #include "simstruc.h"


// ---- Constants & Macros ---- //

// pi
#define PI 3.14159265359
#define PI_2 (PI/2.0)
#define DEG_TO_RAD (PI/180.0)

// motors indexes
enum{RIGHT_MOT, LEFT_MOT}; 


// ---- Functions Prototypes ---- //

// initialization and main loop
#ifdef __cplusplus
extern "C" {
#endif
	void controller_init(ControllerStruct *cvs);
	void controller_loop(ControllerStruct *cvs);
#ifdef __cplusplus
}
#endif


// controller functions
void compute_wheel_references(ControllerStruct *cvs);
void proportional_controller(ControllerStruct *cvs);
double linear_interpol(double y_0, double y_end, double t_0, double t_end, double t);

/*--------------------*/
#endif
