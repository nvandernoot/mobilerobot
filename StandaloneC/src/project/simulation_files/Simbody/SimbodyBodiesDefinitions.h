/*
 * Simbody bodies definition
 *
 * authors: Alexandra Zobova & Timothee Habra
 */

#ifdef SIMBODY

// number of bodies with contact
#define NB_CONTACT_BODIES 0

// S sensors
// #define S_SENSOR_EXAMPLE 1

// F sensors
#define F_SENSOR_ROBOT 1

// sensors tabulars
#define S_SENSORS_ARRAY {}
#define F_SENSORS_ARRAY {F_SENSOR_ROBOT}

// path to the .obj mesh files
#define BODIES_OBJ_PATH PROJECT_ABS_PATH,"/src/project/simulation_files/Simbody/mesh_obj"

#endif
