/*===========================================================================*
  *
  *  user_sf_IO.h
  *	
  *  Project:	MobileRobot
  * 
  *  Generation date: 14-Oct-2014 22:47:05
  * 
  *  (c) Universite catholique de Louvain
  *      D�partement de M�canique 
  *      Unit� de Production M�canique et Machines 
  *      2, Place du Levant 
  *      1348 Louvain-la-Neuve 
  *  http://www.robotran.be// 
  *  
 /*===========================================================================*/

#ifndef UsersfIO_h
#define UsersfIO_h
/*--------------------*/
 
#ifdef ACCELRED 
#define S_FUNCTION_NAME  mbs_sf_accelred_MobileRobot 
#elif defined DIRDYNARED 
#define S_FUNCTION_NAME  mbs_sf_dirdynared_MobileRobot 
#elif defined INVDYNARED 
#define S_FUNCTION_NAME  mbs_sf_invdynared_MobileRobot 
#elif defined SENSORKIN 
#define S_FUNCTION_NAME  mbs_sf_sensorkin_MobileRobot 
#endif 
 
#define SF_N_USER_INPUT 0 
#define SF_N_USER_OUTPUT 3 

#include "userDef.h"
#include "ControllersStruct.h"
 
typedef struct UserIOStruct 
{
    double voltage[2+1];
    int nb_circle_obstacles;
    double circle_obstacles_list[3+1][3+1];
    double walls_list[4+1];
    double tsim_out;
    double out[10+1];
    int stop_simu;
    struct ControllerStruct *cvs;

    #ifdef SIMBODY
    SimbodyStruct *simbodyStruct;
    #endif

} UserIOStruct;

/*--------------------*/
#endif
