//--------------------------- 
// UCL-CEREM-MBS 
// 
// @version MBsysLab_s 1.7.a 
// 
// Creation : 2006 
// Last update : 01/10/2008 
//--------------------------- 

#include "simu_def.h"

#ifdef SIMBODY
#include "simbody_functions.h"
#include "simbody_cpp_functions.h"
#endif

double* user_ExtForces(double PxF[4], double RxF[4][4], 
					   double VxF[4], double OMxF[4], 
					   double AxF[4], double OMPxF[4], 
					   MBSdataStruct *MBSdata, double tsim, int ixF)
{
	int i;
	double Fx=0.0, Fy=0.0, Fz=0.0;
	double Mx=0.0, My=0.0, Mz=0.0;
	double dxF[3+1] ={0.0, 0.0, 0.0, 0.0}; // +1 because indexes begin at 1

	#ifdef SIMBODY
	SimbodyBodiesStruct *simbodyBodies;
	#endif

	double *SWr = MBSdata->SWr[ixF];
	int idpt = 0;
        
    // variables declaration
    UserIOStruct *uvs;

    double x_robot, y_robot;
	double xd_robot, yd_robot;
	double x_obstacle, y_obstacle, R_obstacle;

	double diff_x, diff_y;
	double dist, dist_min_R;
	double dist_wall;

	double contact_velocity, contact_angle;
	double F_abs;

    // varibales initialization
    uvs = MBSdata->user_IO;

	idpt = MBSdata->xfidpt[ixF];

	dxF[1] = MBSdata->dpt[1][idpt];
	dxF[2] = MBSdata->dpt[2][idpt];
	dxF[3] = MBSdata->dpt[3][idpt];

	// Computes the total force to apply on the center of the robot
	// when it impacts obstacles.

	switch(ixF)
	{
		case F_ROBOT_EXT_FORCE: // contact sensor

			// robot position
			x_robot = PxF[1]; // x position [m]
			y_robot = PxF[2]; // y position [m]

			// robot velocity [m/s]
			xd_robot = VxF[1];
			yd_robot = VxF[2];
			
			// loop on all the circle obstacles
			for(i=1; i<=uvs->nb_circle_obstacles; i++)
			{
				// obstacle position and radius
				x_obstacle = uvs->circle_obstacles_list[i][1]; // x position [m]
				y_obstacle = uvs->circle_obstacles_list[i][2]; // y position [m]
				R_obstacle = uvs->circle_obstacles_list[i][3]; // radius [m]

				// distances [m]
				diff_x = x_robot - x_obstacle;
				diff_y = y_robot - y_obstacle;

				// distance between their centers [m]
				dist = sqrt( diff_x*diff_x + diff_y*diff_y );

				// shortest distance between their borders [m] (negative if contact)
				dist_min_R = dist - R_ROBOT - R_obstacle;

				// contact happening
				if (dist_min_R < 0.0)
				{
					// derivative of 'dist_min_R' [m/s]
					contact_velocity = ( diff_x * xd_robot + diff_y * yd_robot ) / dist;

					// absolute angle of contact [rad]
					contact_angle = atan2(y_robot - y_obstacle , x_robot - x_obstacle); 

					// force to expel the body [N] (like a spring with damping)
					F_abs = -(K_CONTACT * dist_min_R + KD_CONTACT * contact_velocity);

					// x and y components of the force [N]
					Fx += F_abs * cos(contact_angle);
            		Fy += F_abs * sin(contact_angle);
				}
			}


			// walls external forces

	 		// 1: repel for x > ...
			dist_wall = uvs->walls_list[1] - R_ROBOT - x_robot;

			if (dist_wall < 0.0)
			{
				Fx += (K_CONTACT * dist_wall - KD_CONTACT * xd_robot);
			}

			// 2: repel for x < ...
			dist_wall = x_robot - uvs->walls_list[2] - R_ROBOT;

			if (dist_wall < 0.0)
			{
				Fx -= (K_CONTACT * dist_wall + KD_CONTACT * xd_robot);
			}

			// 3: repel for y > ...
			dist_wall = uvs->walls_list[3] - R_ROBOT - y_robot;

			if (dist_wall < 0.0)
			{
				Fy += (K_CONTACT * dist_wall - KD_CONTACT * yd_robot);
			}

			// 4: repel for y < ...
			dist_wall = y_robot - uvs->walls_list[4] - R_ROBOT;

			if (dist_wall < 0.0)
			{
				Fy -= (K_CONTACT * dist_wall + KD_CONTACT * yd_robot);
			}

			break;
	
		default:
			break;
	}

	// for Simbody contacts
	#ifdef SIMBODY
	
	//compute all the forces at once (arbitrary for 1st external force) (else compute same thing for each force sensors)
	if(ixF == 1)   
	{
		// 1) Simbody receives kinematics from Robotran
		update_simbody_kinematics(MBSdata->user_IO->simbodyStruct->simbodyBodies, MBSdata);

		// 2) Simbody computes contact force
		loop_Simbody(MBSdata->user_IO->simbodyStruct);
	}

	// 3) Simbody sends contact forces to robotran dynamics
	simbodyBodies = uvs->simbodyStruct->simbodyBodies;
		
	for(i=0; i<simbodyBodies->nb_contact_bodies; i++)
	{
	 	if (ixF == simbodyBodies->F_sensor_Robotran_index[i])
	 	{
	 		Fx += simbodyBodies->force_bodies[i][0]; 
	 		Fy += simbodyBodies->force_bodies[i][1]; 
	 		Fz += simbodyBodies->force_bodies[i][2]; 

	 		Mx += simbodyBodies->torque_bodies[i][0]; 
	 		My += simbodyBodies->torque_bodies[i][1]; 
	 		Mz += simbodyBodies->torque_bodies[i][2];  		

	 		break;
	 	}
	}
	#endif

	// initializes the terms of Swr
	SWr[1] = Fx;
	SWr[2] = Fy;
	SWr[3] = Fz;
	SWr[4] = Mx;
	SWr[5] = My;
	SWr[6] = Mz;
	SWr[7] = dxF[1];
	SWr[8] = dxF[2];
	SWr[9] = dxF[3];

	return SWr;
}
