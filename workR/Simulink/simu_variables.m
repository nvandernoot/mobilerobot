function [simu_vars_none, simu_vars_in, simu_vars_out, simu_vars_struct] = simu_variables()

% Definition of the simulation variables and I/O ports (available in MBSdata->user_IO)
% For each line: varname , type , size
% 
% 4 types of simulation variables:
%       . simu_vars_none   : normal variables
%       . simu_vars_in     : user inputs coming from the Matlab environment
%       . simu_vars_out    : user outputs to analyse results in Matlab
%       . simu_vars_struct : structures
%
% their corresponding type fields:
%       . none   = internal variable (type: 'int'/'double')
%       . in     = input (type: 'int'/'double')
%       . out    = output (type: 'int'/'double')
%       . struct = structure variable (type: structure name with '')
% 
%   - varname = name of the port or of the variable (with '')
%
%   - size: number of elements in the vector
%       1    :  simple variable
%       n    :  vector of n (n>1) elements
%       [m n]:  tabular of 2 entries with a size m*n 
%       0    :  forbidden
%       <1   :  pointer -> fabs(x) = number of stars
%          indexes start at 1 -> different from 'control_variables' 
%

simu_vars_none = {
    'voltage','double',2
    'nb_circle_obstacles','int',1
    'circle_obstacles_list','double',[3 3]
    'walls_list','double',4
};

simu_vars_in = {
};

simu_vars_out = {
    'tsim_out','double',1
    'out','double',10
};

simu_vars_struct = {
    'cvs','ControllerStruct',1
};

end
