//---------------------------
// Nicolas Van der Noot
//
// Creation : 19-Sep-2013
// Last update : 14-Oct-2014
//---------------------------

#ifndef ControllerStruct_h
#define ControllerStruct_h


// ---- Structures definitions (typedef) ---- //

// ControllerInputsStruc
typedef struct ControllerInputs
{
    double t;
    double r_wheel_vel;
    double l_wheel_vel;

} ControllerInputs;


// ControllerOutputsStruc
typedef struct ControllerOutputs
{
    double voltage[2];

} ControllerOutputs;


// VelocityReferencesStruc
typedef struct VelocityReferences
{
    double r_wheel;
    double l_wheel;
    int keyboard_refs[2];

} VelocityReferences;


// ControllerStructStruc
typedef struct ControllerStruct
{
    struct ControllerInputs *inputs;
    struct ControllerOutputs *outputs;
    struct VelocityReferences *vel_ref;
    double motor_commands[2];

} ControllerStruct;


// ---- Init and free functions: declarations ---- //

ControllerInputs * init_ControllerInputs(void);
void free_ControllerInputs(ControllerInputs *cvs);

ControllerOutputs * init_ControllerOutputs(void);
void free_ControllerOutputs(ControllerOutputs *cvs);

VelocityReferences * init_VelocityReferences(void);
void free_VelocityReferences(VelocityReferences *cvs);

ControllerStruct * init_ControllerStruct(void);
void free_ControllerStruct(ControllerStruct *cvs);

/*--------------------*/
#endif

