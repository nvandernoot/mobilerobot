//---------------------------
// Nicolas Van der Noot
//
// Creation : 29/10/2013
// Last update : 29/10/2013
//
// Main loop of the controller and user simulation files
//
//---------------------------

#include "simu_def.h"

void simu_controller_loop(MBSdataStruct *MBSdata)
{
    // variables declaration
    UserIOStruct     *uvs;
    ControllerStruct *cvs;
    
    // variables initialization
    uvs = MBSdata->user_IO;
	cvs = uvs->cvs;

    // controller
    controller_loop_interface(MBSdata);

    // simulation outputs
    simulink_outputs(MBSdata);

	// stopping the simulation if needed
	stop_simu(MBSdata);
}
