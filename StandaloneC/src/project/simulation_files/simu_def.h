//---------------------------
// Nicolas Van der Noot
//
// Creation : 29/10/2013
// Last update : 29/06/2014
//
// Simulation files main header
//
//---------------------------

#ifndef simulation_def_h
#define simulation_def_h
//--------------------*/

#include <math.h>
#include <stdlib.h>

#include "user_sf_IO.h"
#include "MBSdef.h"
#include "MBSfun.h"
#include "MBSdataStruct.h"
#include "ControllersStruct.h"
#include "nrutil.h"


// ---- Constants & Macros ---- //


// -- Joints numbering -- //

// -> to get them, use the file workR/Simulink/get_indexes.m

// floating base indexes
#define FJ_T1 1
#define FJ_T2 2
#define FJ_R3 3

// robot joints indexes
#define RIGHT_WHEEL_JOINT 4
#define LEFT_WHEEL_JOINT  5


// -- Sensors -- //

// S sensors indexes
// #define S_SENSOR_EXAMPLE 1

// F sensors indexes
#define F_ROBOT_EXT_FORCE 1 // F sensor at the center of the robot for contact model

// -- Other -- //

// user derivatives indexes
#define UX_ROLLING_T1 1
#define UX_ROLLING_T2 2
#define UX_ROLLING_R3 3

// robot properties
#define SEMI_AXLE    0.09    // half of the distance between the two wheels [m]
#define WHEEL_RADIUS 0.035   // wheel radius [m]
#define R_ROBOT      0.1     // robot body radius [m]

// motor parameters
#define K_PHI_MOT 0.035  // torque constant [Nm/A]
#define R_MOT     20     // motor resistance [ohm] 
#define RHO_MOT   20     // motor reductor [-]
#define L_MOT     3.0e-4 // unused: inductance neglected [H]

// motor indexes for the Controller
#define CTRL_RWHEEL 0
#define CTRL_LWHEEL 1

// pi
#define PI 3.14159265359
#define PI_2 (PI/2.0)

// contacts properties (spring with damper model)
#define K_CONTACT  1.0e5 // stiffness [N/m]
#define KD_CONTACT 1.0e3 // damping   [N s/m]

// normalized command bounds
#define MIN_COMMAND -100.0 // min normalized command [-]
#define MAX_COMMAND  100.0 // max normalized command [-]
#define DIFF_COMMAND (MAX_COMMAND - MIN_COMMAND)
#define MEAN_COMMAND ((MIN_COMMAND + MAX_COMMAND) / 2.0)
#define SEMI_DIFF_COMMAND (DIFF_COMMAND / 2.0)

// motor minimum and maximum voltages
#define MIN_VOLTAGE -10.0 // min voltage [V]
#define MAX_VOLTAGE  10.0 // max voltage [V]
#define DIFF_VOLTAGE (MAX_VOLTAGE - MIN_VOLTAGE)
#define MEAN_VOLTAGE ((MIN_VOLTAGE + MAX_VOLTAGE) / 2.0)
#define SEMI_DIFF_VOLTAGE (DIFF_VOLTAGE / 2.0)

// motors numbering (only for the simulation, not available in the controller)
enum{R_MOTOR, L_MOTOR}; 

// ---- Custom Functions ---- //

// simulation loop
void simulink_outputs(MBSdataStruct *MBSdata);
void simu_controller_loop(MBSdataStruct *MBSdata);
void stop_simu(MBSdataStruct *MBSdata);

#ifdef STANDALONE
void user_finalization(MBSdataStruct *MBSdata);
#endif

// controller interface
void controller_init_interface(MBSdataStruct *MBSdata);
void controller_loop_interface(MBSdataStruct *MBSdata);
void controller_close_interface(MBSdataStruct *MBSdata);

// custom functions
double limit_value(double val, double min_val, double max_val);

/*--------------------*/
#endif
