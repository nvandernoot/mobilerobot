%
%-------------------------------------------------------------
%
%	ROBOTRAN - Version 6.6 (build : february 22, 2008)
%
%	Copyright 
%	Universite catholique de Louvain 
%	Departement de Mecanique 
%	Unite de Production Mecanique et Machines 
%	2, Place du Levant 
%	1348 Louvain-la-Neuve 
%	http://www.robotran.be// 
%
%	==> Generation Date : Thu Jul 10 18:05:24 2014
%
%	==> Project name : MobileRobot
%	==> using XML input file 
%
%	==> Number of joints : 5
%
%	==> Function : F19 : External Forces
%	==> Flops complexity : 18
%
%	==> Generation Time :  0.000 seconds
%	==> Post-Processing :  0.000 seconds
%
%-------------------------------------------------------------
%
function [frc,trq] = extforces(s,tsim,usrfun)

 frc = zeros(3,5);
 trq = zeros(3,5);

q = s.q; 
qd = s.qd; 
qdd = s.qdd; 
frc = s.frc; 
trq = s.trq; 

% === begin imp_aux === 

% === end imp_aux === 

% ===== BEGIN task 0 ===== 
 
% Sensor Kinematics 



% = = Block_0_0_0_0_0_1 = = 
 
% Trigonometric Variables  

  C3 = cos(q(3));
  S3 = sin(q(3));

% = = Block_0_0_1_1_0_1 = = 
 
% Sensor Kinematics 


  PxF1(1) = q(1);
  PxF1(2) = q(2);
  PxF1(3) = s.dpt(3,1);
  RxF1(1,1) = C3;
  RxF1(1,2) = S3;
  RxF1(1,3) = 0;
  RxF1(2,1) = -S3;
  RxF1(2,2) = C3;
  RxF1(2,3) = 0;
  RxF1(3,1) = 0;
  RxF1(3,2) = 0;
  RxF1(3,3) = (1.0);
  VxF1(1) = qd(1);
  VxF1(2) = qd(2);
  VxF1(3) = 0;
  OMxF1(1) = 0;
  OMxF1(2) = 0;
  OMxF1(3) = qd(3);
  AxF1(1) = qdd(1);
  AxF1(2) = qdd(2);
  AxF1(3) = 0;
  OMPxF1(1) = 0;
  OMPxF1(2) = 0;
  OMPxF1(3) = qdd(3);
 
% Sensor Forces Computation 

  SWr1 = usrfun.fext(PxF1,RxF1,VxF1,OMxF1,AxF1,OMPxF1,s,tsim,1);
 
% Sensor Dynamics : Forces projection on body-fixed frames 

  xfrc11 = SWr1(1)*C3+SWr1(2)*S3;
  xfrc21 = -(SWr1(1)*S3-SWr1(2)*C3);
  xfrc31 = SWr1(3);
  frc(1,3) = s.frc(1,3)+xfrc11;
  frc(2,3) = s.frc(2,3)+xfrc21;
  frc(3,3) = s.frc(3,3)+SWr1(3);
  xtrq11 = SWr1(4)*C3+SWr1(5)*S3;
  xtrq21 = -(SWr1(4)*S3-SWr1(5)*C3);
  xtrq31 = SWr1(6);
  trq(1,3) = s.trq(1,3)+xtrq11-xfrc21*(SWr1(9)-s.l(3,3))+xfrc31*SWr1(8);
  trq(2,3) = s.trq(2,3)+xtrq21+xfrc11*(SWr1(9)-s.l(3,3))-xfrc31*(SWr1(7)-s.l(1,3));
  trq(3,3) = s.trq(3,3)+xtrq31-xfrc11*SWr1(8)+xfrc21*(SWr1(7)-s.l(1,3));

% = = Block_0_0_1_1_1_0 = = 
 
% Symbolic Outputs  

  frc(1,4) = s.frc(1,4);
  frc(2,4) = s.frc(2,4);
  frc(3,4) = s.frc(3,4);
  frc(1,5) = s.frc(1,5);
  frc(2,5) = s.frc(2,5);
  frc(3,5) = s.frc(3,5);
  trq(1,4) = s.trq(1,4);
  trq(2,4) = s.trq(2,4);
  trq(3,4) = s.trq(3,4);
  trq(1,5) = s.trq(1,5);
  trq(2,5) = s.trq(2,5);
  trq(3,5) = s.trq(3,5);

% ====== END Task 0 ====== 

  

