//---------------------------
//
// Nicolas Van der Noot
//
// Creation : 29/10/2013
// Last update : 29/06/2014
//---------------------------

#include "simu_def.h"

void controller_inputs(MBSdataStruct *MBSdata)
{
	// -- Variables declaration -- //

    UserIOStruct     *uvs;
    ControllerInputs *ivs;


    // -- Variables initialization -- //

    uvs = MBSdata->user_IO;
    ivs = uvs->cvs->inputs;
    

    // -- Filling the inputs fields -- //
    
    // time [s]
    ivs->t = MBSdata->tsim;

    // wheels measured velocities [rad/s]
	ivs->r_wheel_vel = MBSdata->qd[RIGHT_WHEEL_JOINT];
	ivs->l_wheel_vel = MBSdata->qd[LEFT_WHEEL_JOINT];
}
