function [controllers_struct] = control_variables()

% Definition of the control state variables 
%      (available in MBSdata->user_IO->cvs if you add the line ''cvs','ControllerStruct',1' in simu_variables.m)
%
% For each line: varname , type , size
%
%  - varname: name of the variable (with '')
%
%  - type: int / double / name of the structure
%
%  - size: number of elements in the vector
%       1    :  simple variable
%       n    :  vector of n (n>1) elements
%       [m n]:  tabular of 2 entries with a size m*n 
%       0    :  forbidden
%       <1   :  pointer -> fabs(x) = number of stars
% indexes start at 0 -> different from 'simu_variables'
%
% You can use recursive structures. 
% In this case, you must define sub-structures before their parent structure.
%

%% controller_1

controller_1_name = 'ControllerInputs';
controller_1_vars = {
    't','double',1
    'r_wheel_vel','double',1
    'l_wheel_vel','double',1
};


%% controller_2

controller_2_name = 'ControllerOutputs';
controller_2_vars = {
    'voltage','double',2
};


%% controller_3

controller_3_name = 'VelocityReferences';
controller_3_vars = {
    'r_wheel','double',1
    'l_wheel','double',1
    'keyboard_refs','int',2
};


%% controller_4

controller_4_name = 'ControllerStruct';
controller_4_vars = {
    'inputs','ControllerInputs',1
    'outputs','ControllerOutputs',1
    'vel_ref','VelocityReferences',1
    'motor_commands','double',2
};


%% controllers_vars

controllers_struct = {
    controller_1_name, controller_1_vars;
    controller_2_name, controller_2_vars;
    controller_3_name, controller_3_vars;
    controller_4_name, controller_4_vars;
};

end
