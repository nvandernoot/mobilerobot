//--------------------------- 
// UCL-CEREM-MBS 
// 
// @version MBsysLab_s 1.7.a 
// 
// Creation : 2008 
// Last update : 24/10/2008 
//--------------------------- 
 
#include "simu_def.h"

// User parameters initialization
#ifndef STANDALONE
void user_initialization(SimStruct *S, MBSdataStruct *MBSdata, LocalDataStruct *lds)
#else
// Returns 0 if no problem
int user_initialization(MBSdataStruct *MBSdata, LocalDataStruct *lds)
#endif
{  
	UserIOStruct *uvs;

	uvs = MBSdata->user_IO;


	// -- Circle obstacles -- //

	// number of circle obstacles
	uvs->nb_circle_obstacles = 3;


	/* obstacles description:
	 *    first index: obstacle index (starting at 1)
	 *    second index: 
	 *        1: x obstacle center position [m]
	 *        2: y obstacle center position [m]
	 *        3: obstacle radius [m]
	 */

	// totem right
	uvs->circle_obstacles_list[1][1] =  0.0;
	uvs->circle_obstacles_list[1][2] = -0.4;
	uvs->circle_obstacles_list[1][3] =  0.12;

	// totem left
	uvs->circle_obstacles_list[2][1] = 0.0;
	uvs->circle_obstacles_list[2][2] = 0.4;
	uvs->circle_obstacles_list[2][3] = 0.12;

	// tree
	uvs->circle_obstacles_list[3][1] = 0.0;
	uvs->circle_obstacles_list[3][2] = 0.0;
	uvs->circle_obstacles_list[3][3] = 0.02;



	// -- Walls -- //

	/*
	 * walls description: 4 walls
	 *    1: repel for x > ...
	 *    2: repel for x < ...
	 *    3: repel for y > ...
	 *    4: repel for y < ...
	 *
	 * you must provide the corresponding position for these 4 walls (in [m])
	 */
	uvs->walls_list[1] =  1.0;
	uvs->walls_list[2] = -1.0;
	uvs->walls_list[3] =  1.5;
	uvs->walls_list[4] = -1.5;



	// -- Controller initialization -- //
    
    controller_init_interface(MBSdata);


    #ifdef STANDALONE
    return 0;
    #endif
}
