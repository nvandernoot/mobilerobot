//---------------------------
// Nicolas Van der Noot & Allan Barrea
//
// Creation : 29/10/2013
// Last update : 29/10/2013
//
// Stop the simulation if needed
//
//---------------------------

#include "simu_def.h"

/*
 * Stop simulation if uvs->stop_simu == 1
 */
void stop_simu(MBSdataStruct *MBSdata)
{
	// --- Variables declaration --- //
    
    // user variables
    UserIOStruct *uvs;
    
	// --- Variables initialization and memory allocation --- //
    
    uvs = MBSdata->user_IO;

	// --- Writing output --- //
    uvs->stop_simu = 0;
}
