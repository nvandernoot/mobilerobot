

function [h,Jac] = user_cons_hJ(mbs_data,tsim)
% --------------------------
% UCL-CEREM-MBS
%
% @version MBsysLab_m 1.7.a
%
% Creation : 2005
% Last update : 30/09/2008
% -------------------------
%
%[h,Jac] = user_cons_hJ(mbs_data,tsim)
%
% mbs_data : multibody data structure
% tsim     : current time
%
% h   : vector of user constraints residue 
% Jac : Jacobian matrix of the user constraints
%   note :
%   d(h)/dt = hd(q,qd) = Jac*qd
%   d2(h)/dt2 = hdd(q,qd,qdd) = J*qdd + Jdqd

global MBS_user MBS_info

q   = mbs_data.q;

Nuc = mbs_data.Nuserc;
Njoint = mbs_data.Njoint;

h   = zeros(Nuc,1);
Jac = zeros(Nuc,Njoint);

%/*-- Begin of user code --*/

% The rolling without slipping equations are computed in this file
% and in the 'user_cons_jdqd.m' file. 

% get the floating base joint indexes
FJ_T1_id = mbs_get_joint_id(MBS_info,'FJ_T1');
FJ_T2_id = mbs_get_joint_id(MBS_info,'FJ_T2');
FJ_R3_id = mbs_get_joint_id(MBS_info,'FJ_R3');

% get the independent joints indexes
Right_axle_id = mbs_get_joint_id(MBS_info,'Right_axle');
Left_axle_id  = mbs_get_joint_id(MBS_info,'Left_axle');

% robot properties
SEMI_AXLE    = 0.09;  % half of the distance between the two wheels [m]
WHEEL_RADIUS = 0.035; % wheel radius [m]

theta   = q(FJ_R3_id);
c_theta = cos(theta);
s_theta = sin(theta);

% computation of h and of the Jacobian
h(1) = 0;
Jac(1,FJ_T1_id)      = -s_theta;
Jac(1,FJ_T2_id)      = c_theta;
Jac(1,FJ_R3_id)      = 0.0;
Jac(1,Left_axle_id)  = 0.0;
Jac(1,Right_axle_id) = 0.0;

h(2) = 0;
Jac(2,FJ_T1_id)      = c_theta;
Jac(2,FJ_T2_id)      = s_theta;
Jac(2,FJ_R3_id)      = -SEMI_AXLE;
Jac(2,Left_axle_id)  = -WHEEL_RADIUS;
Jac(2,Right_axle_id) = 0.0;

h(3) = 0;
Jac(3,FJ_T1_id)      = c_theta;
Jac(3,FJ_T2_id)      = s_theta;
Jac(3,FJ_R3_id)      = SEMI_AXLE;
Jac(3,Left_axle_id)  = 0.0;
Jac(3,Right_axle_id) = -WHEEL_RADIUS;

%/*-- End of user code --*/

return
