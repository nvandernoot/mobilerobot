%--------------------------------------------------------------------------
%   Universite catholique de Louvain
%   CEREM : Centre for research in mechatronics
%   http://www.robotran.be  
%   Contact : robotran@prm.ucl.ac.be
%   Version : ROBOTRAN $Version$
%
%   MBsysLab main script template:
%      - featuring default options
%      - to be adapted by the user
%
%   Project : MobileRobot
%   Author  : Nicolas Van der Noot
%   Date    : 06/07/2014 
%--------------------------------------------------------------------------

%% 1. Initialization and Project Loading [mbs_load]
%--------------------------------------------------------------------------
close all; clear variables; clc;                                            % Cleaning of the Matlab workspace
global MBS_user;                                                            % Declaration of the global user structure
MBS_user.process = '';                                                      % Initialisation of the user field "process"

% Project loading
prjname = 'MobileRobot';
[mbs_data, mbs_info] = mbs_load(prjname,'default');                         % Option 'default': automatic loading of "$project_name$.mbs" 
mbs_data_ini = mbs_data;                                                    % Backup of the initial multibody data structure
                                                                            % Have a look at the content of the mbs_data structure on www.robotran.be

% number of constraints 
% -> if there is no constraint, you must comment these lines 
%     ( 'mbs_data.Nuserc = 0;' might not work !!!)
mbs_data.Nuserc = 3;
mbs_data.Ncons  = 3;


%% 2. Coordinate partitioning [mbs_exe_part]             % For constrained MBS only
%--------------------------------------------------------------------------
MBS_user.process = 'part';

opt.part = {'rowperm','yes','threshold',1e-9,'verbose','yes'};
[mbs_part,mbs_data] = mbs_exe_part(mbs_data,opt.part);

% Coordinate partitioning results
disp('Coordinate partitioning results');
disp(['Sorted independent variables = ', mat2str(mbs_part.ind_u)]);
disp(['Permutated dependent variables = ', mat2str(mbs_part.ind_v)]);
disp(['Permutated independent constraints = ', mat2str(mbs_part.hu)]);
disp(['Redundant constraints = ', mat2str(mbs_part.hv)]);
disp(' ');


%% 3. Direct dynamics [mbs_exe_dirdyn]
%--------------------------------------------------------------------------
MBS_user.process = 'dirdyn';
mbs_data = mbs_set_qu(mbs_data,mbs_data_ini.qu);                            % Retrieving of the initial set of independent variables

t_start   = 0.0;    % start time  [s]
t_finish  = 5.0;    % finish time [s]
time_step = 1.0e-3; % time step   [s]

opt.dirdyn = {'time',t_start:time_step:t_finish,'motion','simulation',...
    'odemethod','ode45','save2file','yes','framerate',1000,...
    'renamefile','no','verbose','yes'};
% other options : 'visualize', 'save2file', 'depinteg', 'dtmax', 'dtinit',
%                 'reltol', 'abstol', 'clearmbsglobal'                      % Help about options on www.robotran.be

% user custom functions to initialize the project
user_initialization(mbs_data);

[mbs_dirdyn,mbs_data] = mbs_exe_dirdyn(mbs_data,opt.dirdyn);                % Direct dynamics process (time simulation)


%% 4. Results analysis
%--------------------------------------------------------------------------

resdirdyn = MBS_user.resdirdyn; % updated in 'user_DirDyn_io.m'

% Graphical Results: Motors
figure;
subplot(2,1,1);
hold on;

% wheel velocities references [rad/s]
plot(mbs_dirdyn.tsim,resdirdyn.vel_ref(:,1),'b'); % right wheel
plot(mbs_dirdyn.tsim,resdirdyn.vel_ref(:,2),'r'); % left wheel

% wheel real speed [rad/s]
plot(mbs_dirdyn.tsim,mbs_dirdyn.qd(:,4),'Color',[0 0.5 0]);   % right wheel
plot(mbs_dirdyn.tsim,mbs_dirdyn.qd(:,5),'Color',[131 15 246]./255); % left wheel

xlabel('time [s]');
ylabel('velocity [rad/s]');
title('Wheels velocities');
legend('ref right','ref left','real right','real left');

% Graphical Results: robot position
subplot(2,1,2);
hold on;
plot(mbs_dirdyn.q(:,1),mbs_dirdyn.q(:,2),'black');                                    
xlabel('x position [m]');
ylabel('y position [m]');
title('Robot position');


%% 5. Closing operations (optional)
%--------------------------------------------------------------------------
mbs_rm_allprjpath;                                                          % Cleaning of the Matlab project paths
mbs_del_glob('MBS_user','MBS_info','MBS_data');                             % Cleaning of the global MBS variables                                                                      % Cleaning of the Matlab command window
