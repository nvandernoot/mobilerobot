/*===========================================================================*
  *
  *  user_sf_IO.c
  *	
  *  Project:	MobileRobot
  * 
  *  Generation date: 14-Oct-2014 22:47:05
  * 
  *  (c) Universite catholique de Louvain
  *      D�partement de M�canique 
  *      Unit� de Production M�canique et Machines 
  *      2, Place du Levant 
  *      1348 Louvain-la-Neuve 
  *  http://www.robotran.be// 
  *  
 /*===========================================================================*/

#include "MBSfun.h" 
#include "user_sf_IO.h" 
#include "sfdef.h" 
#include "userDef.h"
#include "ControllersStruct.h"


UserIOStruct * initUserIO(MBSdataStruct *s)
{
    UserIOStruct *uvs;

    int i, j;

    uvs = (UserIOStruct*) malloc(sizeof(UserIOStruct));

    for (i=1;i<=2;i++)
    {
        uvs->voltage[i] = 0.0;
    }

    uvs->nb_circle_obstacles = 0;

    for (i=1;i<=3;i++)
    {
        for (j=1;j<=3;j++)
        {
            uvs->circle_obstacles_list[i][j] = 0.0;
        }
    }

    for (i=1;i<=4;i++)
    {
        uvs->walls_list[i] = 0.0;
    }

    uvs->tsim_out = 0.0;

    for (i=1;i<=10;i++)
    {
        uvs->out[i] = 0.0;
    }

    uvs->stop_simu = 0;

    uvs->cvs = init_ControllerStruct();

    // simbodyStruct //
    #ifdef SIMBODY
    uvs->simbodyStruct = init_SimbodyStruct();
    #endif

    return uvs;
}


void freeUserIO(UserIOStruct *uvs, MBSdataStruct *s)
{

    // ControllerStruct: cvs //
    free_ControllerStruct(uvs->cvs);
    // SimbodyStruct: simbodyStruct //
    #ifdef SIMBODY
    free_SimbodyStruct(uvs->simbodyStruct);
    #endif

    free(uvs);
}

#ifndef CMEX 
 
void sf_set_user_input_sizes(SimStruct *S, MBSdataStruct *MBSdata, int sf_ninput) 
{ 
   if (SF_N_USER_INPUT > 0) { // warning: index starts at sf_ninput 
        // example: ssSetInputPortWidth(S,sf_ninput,10); 
   } 
} 

void sf_set_user_output_sizes(SimStruct *S, MBSdataStruct *MBSdata) 
        // example: ssSetOutputPortWidth(S, SF_NOUTPUT, 10); 
{ 
   if (SF_N_USER_OUTPUT > 0) { // warning: index starts at SF_NOUTPUT 

       /* User output port0 : tsim_out */ 
       ssSetOutputPortWidth(S, SF_NOUTPUT, 1); 

       /* User output port1 : out */ 
       ssSetOutputPortWidth(S, SF_NOUTPUT+1, 10); 

       /* User output port2 : stop_simu */ 
       ssSetOutputPortWidth(S, SF_NOUTPUT+2, 1); 
   } 
} 

void sf_get_user_input(SimStruct *S, MBSdataStruct *MBSdata, LocalDataStruct *lds, int sf_ninput) 
{ 
    // warning: index starts at sf_ninput
    // example: InputRealPtrsType uPtrs0 = ssGetInputPortRealSignalPtrs(S,sf_ninput);
    //          MBSdata->user_IO->var1 = *uPtrs0[0];
} 

void sf_set_user_output(SimStruct *S, MBSdataStruct *MBSdata, LocalDataStruct *lds) 
{ 
    // warning: index starts at SF_NOUTPUT  
    // example: real_T *y0 = ssGetOutputPortRealSignal(S,SF_NOUTPUT); 
    //          *y0 = MBSdata->user_IO->var1;  
    int i;
  real_T *y0 = ssGetOutputPortRealSignal(S,SF_NOUTPUT); 
  real_T *y1 = ssGetOutputPortRealSignal(S,SF_NOUTPUT+1); 
  real_T *y2 = ssGetOutputPortRealSignal(S,SF_NOUTPUT+2); 

   /* User output port0 : tsim_out */ 
   if (ssGetOutputPortConnected(S,SF_NOUTPUT)) 
      *y0 = MBSdata->user_IO->tsim_out; 

   /* User output port1 : out */ 
   if (ssGetOutputPortConnected(S,SF_NOUTPUT+1)) 
      for (i=1;i<=10;i++)
          y1[i-1] = MBSdata->user_IO->out[i]; 

   /* User output port2 : stop_simu */ 
   if (ssGetOutputPortConnected(S,SF_NOUTPUT+2)) 
      *y2 = MBSdata->user_IO->stop_simu; 
} 

#endif 
