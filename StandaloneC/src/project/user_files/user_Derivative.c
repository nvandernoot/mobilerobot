//--------------------------- 
// UCL-CEREM-MBS 
// 
// @version MBsysLab_s 1.7.a 
// 
// Creation : 2006 
// Last update : 01/10/2008 
//--------------------------- 
 
#include "simu_def.h"

void user_Derivative(MBSdataStruct *MBSdata)
{
	// The rolling without slipping equations require to integrate
	// the floating base velocities to get their correponding positions.

	// rolling without slipping: updating dependent variables
	MBSdata->q[FJ_T1] = MBSdata->ux[UX_ROLLING_T1];
	MBSdata->q[FJ_T2] = MBSdata->ux[UX_ROLLING_T2];
	MBSdata->q[FJ_R3] = MBSdata->ux[UX_ROLLING_R3];

	// rolling without slipping: define variables to integrate
	MBSdata->uxd[UX_ROLLING_T1] = MBSdata->qd[FJ_T1];
	MBSdata->uxd[UX_ROLLING_T2] = MBSdata->qd[FJ_T2];
	MBSdata->uxd[UX_ROLLING_R3] = MBSdata->qd[FJ_R3];
}
