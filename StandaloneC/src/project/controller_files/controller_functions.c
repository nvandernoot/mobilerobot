//---------------------------
// Creation : 29/10/2013
// Last update : 04/07/2014
//
// Useful functions
//
//---------------------------

#include "controller_def.h"

#define TIME_1  1.0
#define TIME_2  2.0
#define TIME_3  6.0
#define TIME_4  7.0
#define TIME_5  10.0
#define TIME_6  20.0
#define TIME_7  25.0
#define TIME_8  30.0
#define TIME_9  40.0
#define TIME_10 55.0

#define WHEEL_VEL_1 5.0
#define WHEEL_VEL_2 10.0


/*
 * Wheel velocity references computed for the right and left wheels
 */
void compute_wheel_references(ControllerStruct *cvs)
{
	// variables declaration
	double t;
	VelocityReferences *vel_ref;

	// variables initialization
	t       = cvs->inputs->t;
	vel_ref = cvs->vel_ref;

	// wheel velocity references computed according to the simulation time
	if (t < TIME_1)
	{
		vel_ref->r_wheel = 0.0;
		vel_ref->l_wheel = 0.0;
	}
	else if (t < TIME_2)
	{
		vel_ref->r_wheel = linear_interpol(0.0, WHEEL_VEL_2, TIME_1, TIME_2, t);
		vel_ref->l_wheel = linear_interpol(0.0, WHEEL_VEL_1, TIME_1, TIME_2, t);
	}
	else if (t < TIME_3)
	{
		vel_ref->r_wheel = WHEEL_VEL_2;
		vel_ref->l_wheel = WHEEL_VEL_2;
	}
	else if (t < TIME_4)
	{
		vel_ref->r_wheel = 0.0;
		vel_ref->l_wheel = 0.0;
	}
	else if (t < TIME_5)
	{
		vel_ref->r_wheel = 0.0;
		vel_ref->l_wheel = linear_interpol(0.0, -WHEEL_VEL_1, TIME_4, TIME_5, t);
	}
	else if (t < TIME_6)
	{
		vel_ref->r_wheel = linear_interpol(0.0, WHEEL_VEL_2, TIME_5, TIME_6, t);
		vel_ref->l_wheel = -WHEEL_VEL_1;
	}
	else if (t < TIME_7)
	{
		vel_ref->r_wheel =  WHEEL_VEL_2;
		vel_ref->l_wheel = -WHEEL_VEL_1;
	}
	else if (t < TIME_8)
	{
		vel_ref->r_wheel = linear_interpol( WHEEL_VEL_2, 0.0, TIME_7, TIME_8, t);
		vel_ref->l_wheel = linear_interpol(-WHEEL_VEL_1, 0.0, TIME_7, TIME_8, t);
	}
	else if (t < TIME_9)
	{
		vel_ref->r_wheel = WHEEL_VEL_1;
		vel_ref->l_wheel = WHEEL_VEL_2;
	}
	else if (t < TIME_10)
	{
		vel_ref->r_wheel = linear_interpol(WHEEL_VEL_1, 0.0, TIME_9, TIME_10, t);
		vel_ref->l_wheel = linear_interpol(WHEEL_VEL_2, 0.0, TIME_9, TIME_10, t);
	}
	else
	{
		vel_ref->r_wheel = 0.0;
		vel_ref->l_wheel = 0.0;
	}
}

/*
 * P controller to get the motor commands
 */
void proportional_controller(ControllerStruct *cvs)
{
	// variables declaration
	double k_p;

	ControllerInputs   *ivs;
	VelocityReferences * vel_ref;
	
	// variables initialization
	k_p = 5.0e2; // proportional gain [s/m] for the P controller command 

	ivs     = cvs->inputs;
	vel_ref = cvs->vel_ref;

	// motor commands [-] bounded in [-100 ; 100]
	cvs->motor_commands[RIGHT_MOT] = k_p * (vel_ref->r_wheel - ivs->r_wheel_vel);
	cvs->motor_commands[LEFT_MOT]  = k_p * (vel_ref->l_wheel - ivs->l_wheel_vel);
}


/*
 * Linear interpolation: evaluation of y(t)
 * with y(t_0) = y_0, y(t_end) = y_end
 */
double linear_interpol(double y_0, double y_end, double t_0, double t_end, double t)
{
    if (t < t_0)
    {
        return y_0;
    }
    else if (t < t_end)
    {
        return y_0 + (t - t_0) / (t_end - t_0) * (y_end - y_0);
    }
    else
    {
        return y_end;
    }
}
