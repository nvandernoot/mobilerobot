//
//-------------------------------------------------------------
//
//	ROBOTRAN - Version 6.6 (build : february 22, 2008)
//
//	Copyright 
//	Universite catholique de Louvain 
//	Departement de Mecanique 
//	Unite de Production Mecanique et Machines 
//	2, Place du Levant 
//	1348 Louvain-la-Neuve 
//	http://www.robotran.be// 
//
//	==> Generation Date : Thu Jul 10 18:05:24 2014
//
//	==> Project name : MobileRobot
//	==> using XML input file 
//
//	==> Number of joints : 5
//
//	==> Function : F 1 : Direct Dynamics (Semi-Explicit formulation) : RNEA
//	==> Flops complexity : 209
//
//	==> Generation Time :  0.000 seconds
//	==> Post-Processing :  0.000 seconds
//
//-------------------------------------------------------------
//
 
#include <math.h> 

#include "MBSdataStructR7.h"
#include "MBSfunR7.h"
 
void dirdyna(double **M,double *c,
MBSdataStruct *s, double tsim)

// double M[5][5];
// double c[5];
{ 
 
#include "mbs_dirdyna_MobileRobot.h" 
#define q s->q 
#define qd s->qd 
#define qdd s->qdd 
 
 

// === begin imp_aux === 

// === end imp_aux === 

// ===== BEGIN task 0 ===== 

// = = Block_0_0_0_0_0_1 = = 
 
// Trigonometric Variables  

  C3 = cos(q[3]);
  S3 = sin(q[3]);

// = = Block_0_0_0_0_0_2 = = 
 
// Trigonometric Variables  

  C4 = cos(q[4]);
  S4 = sin(q[4]);

// = = Block_0_0_0_0_0_3 = = 
 
// Trigonometric Variables  

  C5 = cos(q[5]);
  S5 = sin(q[5]);

// = = Block_0_1_0_0_0_1 = = 
 
// Forward Kinematics 

  BS53 = -qd[3]*qd[3];

// = = Block_0_1_0_1_0_2 = = 
 
// Forward Kinematics 

  OM14 = -qd[3]*S4;
  OM34 = qd[3]*C4;

// = = Block_0_1_0_1_0_3 = = 
 
// Forward Kinematics 

  OM15 = -qd[3]*S5;
  OM35 = qd[3]*C5;

// = = Block_0_2_0_1_0_2 = = 
 
// Backward Dynamics 

  FA14 = -(s->frc[1][4]-s->m[4]*s->g[3]*S4);
  FA34 = -(s->frc[3][4]+s->m[4]*s->g[3]*C4);
  CF24 = -(s->trq[2][4]-OM14*OM34*(s->In[1][4]-s->In[9][4]));
  FB14_3 = -s->m[4]*C4*(s->dpt[2][2]+s->l[2][4]);
  FB34_3 = -s->m[4]*S4*(s->dpt[2][2]+s->l[2][4]);

// = = Block_0_2_0_1_0_3 = = 
 
// Backward Dynamics 

  FA15 = -(s->frc[1][5]-s->m[5]*s->g[3]*S5);
  FA35 = -(s->frc[3][5]+s->m[5]*s->g[3]*C5);
  CF25 = -(s->trq[2][5]-OM15*OM35*(s->In[1][5]-s->In[9][5]));
  FB15_3 = -s->m[5]*C5*(s->dpt[2][3]+s->l[2][5]);
  FB35_3 = -s->m[5]*S5*(s->dpt[2][3]+s->l[2][5]);

// = = Block_0_2_0_2_0_1 = = 
 
// Backward Dynamics 

  FF13 = -(s->frc[1][3]+qd[3]*qd[3]*s->m[3]*s->l[1][3]-FA14*C4-FA15*C5-FA34*S4-FA35*S5);
  FF23 = -(s->frc[2][3]+s->frc[2][4]+s->frc[2][5]+s->m[4]*(qd[3]*qd[3]*s->l[2][4]-BS53*s->dpt[2][2])+s->m[5]*(qd[3]*
 qd[3]*s->l[2][5]-BS53*s->dpt[2][3]));
  CF33 = -(s->trq[3][3]+s->frc[2][3]*s->l[1][3]+s->dpt[2][2]*(FA14*C4+FA34*S4)+s->dpt[2][3]*(FA15*C5+FA35*S5)+C4*(
 s->trq[3][4]+qd[3]*qd[4]*s->In[9][4]*S4+qd[4]*OM14*(s->In[1][4]-s->In[5][4])+FA14*s->l[2][4])-S4*(s->trq[1][4]+qd[3]*qd[4]*
 s->In[1][4]*C4+qd[4]*OM34*(s->In[5][4]-s->In[9][4])-FA34*s->l[2][4])+C5*(s->trq[3][5]+qd[3]*qd[5]*s->In[9][5]*S5+qd[5]*OM15*
 (s->In[1][5]-s->In[5][5])+FA15*s->l[2][5])-S5*(s->trq[1][5]+qd[3]*qd[5]*s->In[1][5]*C5+qd[5]*OM35*(s->In[5][5]-s->In[9][5])-
 FA35*s->l[2][5]));
  CM33_1 = -(s->m[3]*s->l[1][3]*S3+s->m[4]*C3*(s->dpt[2][2]+s->l[2][4])+s->m[5]*C3*(s->dpt[2][3]+s->l[2][5]));
  CM33_2 = s->m[3]*s->l[1][3]*C3-s->m[4]*S3*(s->dpt[2][2]+s->l[2][4])-s->m[5]*S3*(s->dpt[2][3]+s->l[2][5]);
  CM33_3 = s->In[9][3]+s->m[3]*s->l[1][3]*s->l[1][3]-s->dpt[2][2]*(FB14_3*C4+FB34_3*S4)-s->dpt[2][3]*(FB15_3*C5+FB35_3*
 S5)+C4*(s->In[9][4]*C4-FB14_3*s->l[2][4])+S4*(s->In[1][4]*S4-FB34_3*s->l[2][4])+C5*(s->In[9][5]*C5-FB15_3*s->l[2][5])+S5*(
 s->In[1][5]*S5-FB35_3*s->l[2][5]);
  FF2_13 = FF13*C3-FF23*S3;
  FF2_23 = FF13*S3+FF23*C3;
  FM21_13 = s->m[3]+s->m[4]+s->m[5];
  FM22_23 = s->m[3]+s->m[4]+s->m[5];

// = = Block_0_3_0_0_0_0 = = 
 
// Symbolic Outputs  

  M[1][1] = FM21_13;
  M[1][3] = CM33_1;
  M[2][2] = FM22_23;
  M[2][3] = CM33_2;
  M[3][1] = CM33_1;
  M[3][2] = CM33_2;
  M[3][3] = CM33_3;
  M[4][4] = s->In[5][4];
  M[5][5] = s->In[5][5];
  c[1] = FF2_13;
  c[2] = FF2_23;
  c[3] = CF33;
  c[4] = CF24;
  c[5] = CF25;

// ====== END Task 0 ====== 


}
 

