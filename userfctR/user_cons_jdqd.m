function [Jdqd] = user_cons_jdqd(mbs_data,tsim)
% --------------------------
% UCL-CEREM-MBS
%
% @version MBsysLab_m 1.7.a
%
% Creation : 2005
% Last update : 30/09/2008
% -------------------------
%
%[Jdqd] = user_cons_jdqd(mbs_data,tsim)
%
% mbs_data : multibody data structure
% tsim     : current time
%
% Jdqd(q,qd) : vector of the user constraints second derivative cross terms
%
%   d(h)/dt = hd(q,qd) = Jac*qd
%   d2(h)/dt2 = hdd(q,qd,qdd) = J*qdd + Jdqd

global MBS_user MBS_info

q   = mbs_data.q;
qd  = mbs_data.qd;

Nuc = mbs_data.Nuserc;

Jdqd = zeros(Nuc,1);

%/*-- Begin of user code --*/

% The rolling without slipping equations are computed in this file
% and in the 'user_cons_hJ.m' file. 

% get the floating base joint indexes
FJ_T1_id = mbs_get_joint_id(MBS_info,'FJ_T1');
FJ_T2_id = mbs_get_joint_id(MBS_info,'FJ_T2');
FJ_R3_id = mbs_get_joint_id(MBS_info,'FJ_R3');

x_p = qd(FJ_T1_id);
y_p = qd(FJ_T2_id);

theta   = q(FJ_R3_id);
theta_p = qd(FJ_R3_id);
c_theta = cos(theta);
s_theta = sin(theta);

xp_tp = x_p*theta_p;
yp_tp = y_p*theta_p;

xp_tp_c = xp_tp*c_theta;
xp_tp_s = xp_tp*s_theta;
yp_tp_c = yp_tp*c_theta;
yp_tp_s = yp_tp*s_theta;

% computation of Jdqd
Jdqd(1) = -xp_tp_c - yp_tp_s;
Jdqd(2) = -xp_tp_s + yp_tp_c;
Jdqd(3) = -xp_tp_s + yp_tp_c;

%/*-- End of user code --*/

return