Read me first !
===============

There are three possibilities to use this simulator:
* Matlab
* Simulink (C)
* Standalone (C/C++)


Matlab version
--------------

This version uses only [Matlab](http://www.mathworks.nl/products/matlab/) files. More information is available on the [robotran website](http://www.robotran.be/), with the following tutorial: [Robotran modeling features](http://www.robotran.be/downloads/tutorial/RobotranLearning_ModelingFeatures.pdf).

No compilation is required for this version. This version is compatible with any computer, provided Matlab is installed (release R2007a or later).
However, this version is the slowest one.


Simulink version (C/C++)
------------------------

This version uses [Simulink](http://www.mathworks.nl/products/simulink/) to compile a C project. So, it is still possible to use some modules coming from Matlab.

C files must be compiled with a [Mex](http://www.mathworks.nl/help/matlab/matlab_external/introducing-mex-files.html) compiler. Thanks to the C code, this version is much faster than the Matlab one (there might be a factor 100 for the execution time). This version can also be extended with C++ code.

Provided Matlab and a Mex compiler are installed, this version can run on the three OS (Linux, Mac OS and Windows), both for the 32-bit and the 64-bit versions.
For more information, consult the [README.md](workR/Simulink/README.md) located in the __workR/Simulink__ folder.


Standalone version (C/C++)
--------------------------

This version is a standalone one in the sense that it does not require Matlab nor Simulink to run. It can be seen as a C function running without resorting to other programs. Nevertheless, some libraries (like [SDL](http://www.libsdl.org/)) are used to add new features. The most important one is its ability to run the simulation in real-time, while allowing the user to interact with it and analyzing results on graphs plotted in real-time.

This version only uses C files but can be extended with C++ code. It uses [CMake](http://www.cmake.org/) to generate projects on any of the three OS (Linux, Mac OS and Windows) with different IDEs ([Visual Studio](http://www.visualstudio.com/), [Code::Blocks](http://www.codeblocks.org/), [XCode](https://developer.apple.com/xcode/)...) or with [Makefiles](http://mrbook.org/tutorials/make/) projects. Nevertheless, this version requires using a 64-bit OS and may have some problems with the real-time visualization on Mac OS. Therefore, we advise to use this version with Linux or Windows 64-bit.

This version requires to compile the project, but is the fastest one (slightly fatster than the Simulink version because there is no link with Simulink-Matlab). For more information, consult the [README.md](StandaloneC/README.md) located in the __StandaloneC__ folder.


Project specific features
-------------------------

This project is a simple template used to present the three versions of the simulator (see above). 

It presents a differential wheeled robot with 2 controlled DC motors (see _user_JointForces.m/.c_). The floating base has 3 dependent variables whose state is updated according to rolling without slipping constraints (see _user_cons_hJ.m/.c_, _user_cons_jdqd.m/.c_, and _user_Derivatives.m/.c_). Finally, a simple 2D contact model between circles and walls is implemented to prevent the robot from colliding with circle obstacles or with walls (see _user_ExtForces.m/.c_). The map is inspired by the 2012 [Eurobot](http://www.eurobot.org/) contest.

In the Matlab or the Simulink version, the voltage sent to the motors is defined before running the simulation. In the Standalone version, you can control the robot with a joystick or with the keyboard. When launching the simulation, the number of plugged joysticks is detected and indicated in the terminal (if any joystick is detected). Keyboard is used when no joystick is detected, otherwise, the first joystick detected is used. The keyboard keys used are the arrows to navigate and the _S_ key to stop. The two first axes of the joystick are used otherwise. These configurations can be modified in _events_config.c_.


__MobileRobot__

[![ScreenShot](Documentation/media/mobile_robot.png)](https://www.youtube.com/watch?v=ywE0b1WRfP4)