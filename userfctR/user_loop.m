function [] = user_loop(mbs_data,tsim,step)
% --------------------------
% UCL-CEREM-MBS
%
% Creation : 2014
% Last update : 06/07/2014
%
% -------------------------
% Function called during the simulation loop.
% User can define its own commands to be executed
% during the simulation main loop.

% Wheel velocity references computed for the right and left wheels
compute_wheel_references(tsim);

% P controller to get the motor commands
proportional_controller(mbs_data);

set_voltage_motors();

end

% Wheel velocity references computed for the right and left wheels
function [] = compute_wheel_references(tsim)

    global MBS_user;

    TIME_1 = 1.0;
    TIME_2 = 1.5;
    TIME_3 = 3.0;
    TIME_4 = 4.0;

    WHEEL_VEL_1 = 5.0;
    WHEEL_VEL_2 = 10.0;

    % wheel velocity references computed according to the simulation time
    if (tsim < TIME_1)

        r_wheel = 0.0;
        l_wheel = 0.0;

    elseif (tsim < TIME_2)

        r_wheel = linear_interpol(0.0, WHEEL_VEL_2, TIME_1, TIME_2, tsim);
        l_wheel = linear_interpol(0.0, WHEEL_VEL_2, TIME_1, TIME_2, tsim);

    elseif (tsim < TIME_3)

        r_wheel = WHEEL_VEL_2;
        l_wheel = WHEEL_VEL_2;

    elseif (tsim < TIME_4)

        r_wheel =  WHEEL_VEL_1;
        l_wheel = -WHEEL_VEL_2;

    else

        r_wheel = 0.0;
        l_wheel = 0.0;

    end
    
    % velocity references for the right (r) and left (l) wheels [rad/s]
    MBS_user.vel_ref_r = r_wheel;
    MBS_user.vel_ref_l = l_wheel;
end


% P controller to get the motor commands
function [] = proportional_controller(mbs_data)

    global MBS_user MBS_info;
    
    qd = mbs_data.qd;

    % proportional gain [s/m] for the P controller command
	k_p = 5.0e2; 
    
    % get the independent joints indexes
    Right_axle_id = mbs_get_joint_id(MBS_info,'Right_axle');
    Left_axle_id  = mbs_get_joint_id(MBS_info,'Left_axle');
    
    r_wheel_ref = MBS_user.vel_ref_r;
    l_wheel_ref = MBS_user.vel_ref_l;

    % motor commands [-] bounded in [-100 ; 100]
	MBS_user.command(1) = k_p * (r_wheel_ref - qd(Right_axle_id));
	MBS_user.command(2) = k_p * (l_wheel_ref - qd(Left_axle_id) );
end


% Computes the two motors voltages
function [] = set_voltage_motors()

    global MBS_user;
    
    % normalized command bounds
    MIN_COMMAND       = -100.0; % min normalized command [-]
    MAX_COMMAND       = 100.0;  % max normalized command [-]
    DIFF_COMMAND      = MAX_COMMAND - MIN_COMMAND;
    MEAN_COMMAND      = (MIN_COMMAND + MAX_COMMAND) / 2.0;
    SEMI_DIFF_COMMAND = DIFF_COMMAND / 2.0;
    
    % motor minimum and maximum voltages
    MIN_VOLTAGE       = -10.0; % min voltage [V]
    MAX_VOLTAGE       = 10.0;  % max voltage [V]
    DIFF_VOLTAGE      = MAX_VOLTAGE - MIN_VOLTAGE;
    MEAN_VOLTAGE      = (MIN_VOLTAGE + MAX_VOLTAGE) / 2.0;
    SEMI_DIFF_VOLTAGE = DIFF_VOLTAGE / 2.0;

    % motor commands [-], bounded in [-100 ; 100]
    MBS_user.command(1) = limit_value(MBS_user.command(1), MIN_COMMAND, MAX_COMMAND);
    MBS_user.command(2) = limit_value(MBS_user.command(2), MIN_COMMAND, MAX_COMMAND);
    
    r_command_normalized = (MBS_user.command(1) - MEAN_COMMAND) / SEMI_DIFF_COMMAND;
    l_command_normalized = (MBS_user.command(2) - MEAN_COMMAND) / SEMI_DIFF_COMMAND;
    
    % motor voltages [V], bounded in [-10 ; 10]
    MBS_user.voltage(1) = r_command_normalized * SEMI_DIFF_VOLTAGE + MEAN_VOLTAGE;
    MBS_user.voltage(2) = l_command_normalized * SEMI_DIFF_VOLTAGE + MEAN_VOLTAGE;
    
    MBS_user.voltage(1) = limit_value(MBS_user.voltage(1), MIN_VOLTAGE, MAX_VOLTAGE);
    MBS_user.voltage(2) = limit_value(MBS_user.voltage(2), MIN_VOLTAGE, MAX_VOLTAGE);

end

% Linear interpolation: evaluation of y(t)
% with y(t_0) = y_0, y(t_end) = y_end
function [y] = linear_interpol(y_0, y_end, t_0, t_end, tsim)

    if (tsim < t_0)
    
        y = y_0;
    
    elseif (tsim < t_end)
    
        y = y_0 + (tsim - t_0) / (t_end - t_0) * (y_end - y_0);
    
    else
    
        y = y_end;
    
    end
end

% Limit value in the [min_value ; max_value] interval
function [y] = limit_value(value, min_value, max_value)

    if (value < min_value)        
        y = min_value;        
    elseif (value > max_value)        
        y = max_value;      
    else        
        y = value;
    end
end
