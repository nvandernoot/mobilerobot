//--------------------------- 
// UCL-CEREM-MBS 
// 
// @version MBsysLab_s 1.7.a 
// 
// Creation : 2006 
// Last update : 01/10/2008 
//--------------------------- 
 
#include "math.h" 
 
#include "MBSdef.h" 
#include "MBSdataStruct.h" 

#include "simu_def.h"
 
 
void user_cons_jdqd(double *jdqd, MBSdataStruct *MBSdata, double tsim) 
{
     
/*-- Begin of user code --*/

    // The rolling without slipping equations are computed in this file
    // and in the 'user_cons_hJ.c' file. 
    
    double x_p, y_p;
    double theta, theta_p, c_theta, s_theta;
    double xp_tp, yp_tp;
    double xp_tp_c, xp_tp_s, yp_tp_c, yp_tp_s;
    
    x_p = MBSdata->qd[FJ_T1];
    y_p = MBSdata->qd[FJ_T2];
    
    theta   = MBSdata->q[FJ_R3];
    theta_p = MBSdata->qd[FJ_R3];
    c_theta = cos(theta);
    s_theta = sin(theta);
    
    xp_tp = x_p*theta_p;
    yp_tp = y_p*theta_p;
    
    xp_tp_c = xp_tp*c_theta;
    xp_tp_s = xp_tp*s_theta;
    yp_tp_c = yp_tp*c_theta;
    yp_tp_s = yp_tp*s_theta;
    
    // computation of Jdqd
    jdqd[1] = -xp_tp_c - yp_tp_s;
    jdqd[2] = -xp_tp_s + yp_tp_c;
    jdqd[3] = -xp_tp_s + yp_tp_c;

/*-- End of user code --*/
    
}
