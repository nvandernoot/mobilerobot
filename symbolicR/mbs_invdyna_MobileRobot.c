//
//-------------------------------------------------------------
//
//	ROBOTRAN - Version 6.6 (build : february 22, 2008)
//
//	Copyright 
//	Universite catholique de Louvain 
//	Departement de Mecanique 
//	Unite de Production Mecanique et Machines 
//	2, Place du Levant 
//	1348 Louvain-la-Neuve 
//	http://www.robotran.be// 
//
//	==> Generation Date : Thu Jul 10 18:05:24 2014
//
//	==> Project name : MobileRobot
//	==> using XML input file 
//
//	==> Number of joints : 5
//
//	==> Function : F 2 : Inverse Dynamics : RNEA
//	==> Flops complexity : 200
//
//	==> Generation Time :  0.000 seconds
//	==> Post-Processing :  0.010 seconds
//
//-------------------------------------------------------------
//
 
#include <math.h> 

#include "MBSdataStructR7.h"
#include "MBSfunR7.h"
 
void invdyna(double *Qq,
MBSdataStruct *s, double tsim)

// double Qq[5];
{ 
 
#include "mbs_invdyna_MobileRobot.h" 
#define q s->q 
#define qd s->qd 
#define qdd s->qdd 
 
 

// === begin imp_aux === 

// === end imp_aux === 

// ===== BEGIN task 0 ===== 

// = = Block_0_0_0_0_0_1 = = 
 
// Trigonometric Variables  

  C3 = cos(q[3]);
  S3 = sin(q[3]);

// = = Block_0_0_0_0_0_2 = = 
 
// Trigonometric Variables  

  C4 = cos(q[4]);
  S4 = sin(q[4]);

// = = Block_0_0_0_0_0_3 = = 
 
// Trigonometric Variables  

  C5 = cos(q[5]);
  S5 = sin(q[5]);

// = = Block_0_1_0_0_0_0 = = 
 
// Forward Kinematics 

  BS53 = -qd[3]*qd[3];
  ALPHA13 = qdd[1]*C3+qdd[2]*S3;
  ALPHA23 = -(qdd[1]*S3-qdd[2]*C3);
  OM14 = -qd[3]*S4;
  OM34 = qd[3]*C4;
  OMp14 = -(qd[3]*qd[4]*C4+qdd[3]*S4);
  OMp34 = -(qd[3]*qd[4]*S4-qdd[3]*C4);
  OM15 = -qd[3]*S5;
  OM35 = qd[3]*C5;
  OMp15 = -(qd[3]*qd[5]*C5+qdd[3]*S5);
  OMp35 = -(qd[3]*qd[5]*S5-qdd[3]*C5);
 
// Backward Dynamics 

  Fs15 = -(s->frc[1][5]-s->m[5]*(s->g[3]*S5-s->l[2][5]*(OMp35-qd[5]*OM15)+C5*(ALPHA13-qdd[3]*s->dpt[2][3])));
  Fs35 = -(s->frc[3][5]+s->m[5]*(s->g[3]*C5-s->l[2][5]*(OMp15+qd[5]*OM35)-S5*(ALPHA13-qdd[3]*s->dpt[2][3])));
  Cq25 = -(s->trq[2][5]-qdd[5]*s->In[5][5]-OM15*OM35*(s->In[1][5]-s->In[9][5]));
  Fs14 = -(s->frc[1][4]-s->m[4]*(s->g[3]*S4-s->l[2][4]*(OMp34-qd[4]*OM14)+C4*(ALPHA13-qdd[3]*s->dpt[2][2])));
  Fs34 = -(s->frc[3][4]+s->m[4]*(s->g[3]*C4-s->l[2][4]*(OMp14+qd[4]*OM34)-S4*(ALPHA13-qdd[3]*s->dpt[2][2])));
  Cq24 = -(s->trq[2][4]-qdd[4]*s->In[5][4]-OM14*OM34*(s->In[1][4]-s->In[9][4]));
  Fs23 = -(s->frc[2][3]-s->m[3]*(ALPHA23+qdd[3]*s->l[1][3]));
  Fq13 = -(s->frc[1][3]-s->m[3]*(ALPHA13-qd[3]*qd[3]*s->l[1][3])-Fs14*C4-Fs15*C5-Fs34*S4-Fs35*S5);
  Fq23 = -(s->frc[2][4]+s->frc[2][5]-Fs23-s->m[4]*(ALPHA23-qd[3]*qd[3]*s->l[2][4]+BS53*s->dpt[2][2])-s->m[5]*(ALPHA23-
 qd[3]*qd[3]*s->l[2][5]+BS53*s->dpt[2][3]));
  Cq33 = -(s->trq[3][3]-qdd[3]*s->In[9][3]-Fs23*s->l[1][3]+s->dpt[2][2]*(Fs14*C4+Fs34*S4)+s->dpt[2][3]*(Fs15*C5+Fs35*S5)
 +C4*(s->trq[3][4]+qd[4]*OM14*(s->In[1][4]-s->In[5][4])-s->In[9][4]*OMp34+Fs14*s->l[2][4])-S4*(s->trq[1][4]+qd[4]*OM34*(
 s->In[5][4]-s->In[9][4])-s->In[1][4]*OMp14-Fs34*s->l[2][4])+C5*(s->trq[3][5]+qd[5]*OM15*(s->In[1][5]-s->In[5][5])-
 s->In[9][5]*OMp35+Fs15*s->l[2][5])-S5*(s->trq[1][5]+qd[5]*OM35*(s->In[5][5]-s->In[9][5])-s->In[1][5]*OMp15-Fs35*s->l[2][5]));
  Fq12 = Fq13*C3-Fq23*S3;
  Fq22 = Fq13*S3+Fq23*C3;

// = = Block_0_2_0_0_0_0 = = 
 
// Symbolic Outputs  

  Qq[1] = Fq12;
  Qq[2] = Fq22;
  Qq[3] = Cq33;
  Qq[4] = Cq24;
  Qq[5] = Cq25;

// ====== END Task 0 ====== 


}
 

