//---------------------------
// Nicolas Van der Noot
//
// Creation : 29/10/2013
// Last update : 06/07/2014
//
// Outputs of the controller
//
//---------------------------

#include "simu_def.h"

/*
 * Limit value 'val' in the [min_val ; max_val] interval.
 */
double limit_value(double val, double min_val, double max_val)
{
	if (val <= min_val)
	{
		return min_val;
	}
	else if (val >= max_val)
	{
		return max_val;
	}
	else
	{
		return val;
	}
}

/*
 * Handles outputs coming from the controller to affect with the simulation environment.
 */
void controller_outputs(MBSdataStruct *MBSdata)
{
    // -- Variables declaration -- //

    UserIOStruct       *uvs;
    ControllerStruct   *cvs;
    ControllerOutputs  *ovs;

    double r_command_normalized, l_command_normalized;


    // -- Variables initialization -- //

    uvs     = MBSdata->user_IO;
    cvs     = uvs->cvs;
    ovs     = cvs->outputs;


    // -- Filling the outputs fields -- //

    // motor commands [-], bounded in [-100 ; 100]
    cvs->motor_commands[R_MOTOR] = limit_value(cvs->motor_commands[R_MOTOR], MIN_COMMAND, MAX_COMMAND);
    cvs->motor_commands[L_MOTOR] = limit_value(cvs->motor_commands[L_MOTOR], MIN_COMMAND, MAX_COMMAND);

    r_command_normalized = (cvs->motor_commands[R_MOTOR] - MEAN_COMMAND) / SEMI_DIFF_COMMAND;
    l_command_normalized = (cvs->motor_commands[L_MOTOR] - MEAN_COMMAND) / SEMI_DIFF_COMMAND;

    // motor voltages [V], bounded in [-10 ; 10]
    ovs->voltage[R_MOTOR] = r_command_normalized * SEMI_DIFF_VOLTAGE + MEAN_VOLTAGE;
    ovs->voltage[L_MOTOR] = l_command_normalized * SEMI_DIFF_VOLTAGE + MEAN_VOLTAGE;

    ovs->voltage[R_MOTOR] = limit_value(ovs->voltage[R_MOTOR], MIN_COMMAND, MAX_COMMAND);
    ovs->voltage[L_MOTOR] = limit_value(ovs->voltage[L_MOTOR], MIN_COMMAND, MAX_COMMAND);
}
