//--------------------------- 
// UCL-CEREM-MBS 
// 
// @version MBsysLab_s 1.7.a 
// 
// Creation : 2006 
// Last update : 01/10/2008 
//--------------------------- 
 
#include "simu_def.h"
 
double* user_JointForces(MBSdataStruct *MBSdata, double tsim)
{ 
	// variables declaration
	double omega_mot_Rwheel, omega_mot_Lwheel;
	double k_phi_r, k_phi_2_r;

    UserIOStruct       *uvs;
    ControllerStruct   *cvs;
    ControllerOutputs  *ovs;

    // variables initialization
	uvs = MBSdata->user_IO;
	cvs = uvs->cvs;
	ovs = cvs->outputs;


	/*
	 * DC motors equations (inductance neglected)
	 * C_load = rho * ((k phi / R) * u_mot - ((k phi * k phi) / R) * (rho * omega_load))
	*/ 

	// motor equations values (better to use macros instead)
	k_phi_r   = K_PHI_MOT / R_MOT;
	k_phi_2_r = k_phi_r * K_PHI_MOT;

	// motors velocities
	omega_mot_Rwheel = MBSdata->qd[RIGHT_WHEEL_JOINT] * RHO_MOT;
	omega_mot_Lwheel = MBSdata->qd[LEFT_WHEEL_JOINT] * RHO_MOT;

	// wheel torques
	MBSdata->Qq[RIGHT_WHEEL_JOINT] = RHO_MOT  * ( k_phi_r * ovs->voltage[R_MOTOR] - k_phi_2_r * omega_mot_Rwheel );
	MBSdata->Qq[LEFT_WHEEL_JOINT]  = RHO_MOT  * ( k_phi_r * ovs->voltage[L_MOTOR] - k_phi_2_r * omega_mot_Lwheel );

    return MBSdata->Qq;
} 
