function Swr = user_ExtForces(PxF,RxF,VxF,OMxF,AxF,OMPxF,mbs_data,tsim,ixF)
% --------------------------
% UCL-CEREM-MBS
%
% @version MBsysLab_m 1.7.a
%
% Creation : 2006
% Last update : 30/09/2008
% -------------------------
%
%Swr = user_ExtForces(PxF,RxF,VxF,OMxF,AxF,OMPxF,mbs_data,tsim,ixF)
%
% PxF(3,1) : absolute position vector of the external force application point 
% RxF(3,3) : absolute rotation matrix of the body
% VxF(3,1) : absolute velocity vector of the external force application point 
% OMxF(3,1) : absolute angular velocity vector of the body
% AxF(3,1) : absolute acceleration vector of the external force application point 
% OMPxF(3,1) : absolute angular acceleration vector of the body
%
% => All above vectors are expressed in the inertial reference frame !
%
% mbs_data : multibody data structure
% tsim : current time
% ixF : index of the external force sensor ('F' type in MBsysPad)
%        (can be obtained via the 'mbs_get_F_sensor_id' function)
%
% Swr(9,1) = [Fx; Fy; Fz; Mx; My; Mz; dxF];
%   - Force components (expressed in the inertial frame) : Fx, Fy, Fz
%   - Pure torque components (expressed in the inertial frame) : Mx, My, Mz
%   - Application point local coordinates vector (expressed in the body-fixed frame) : dxF(1:3,1);
%
% this function may use a global structure called MBS_user

global MBS_user MBS_info

Fx=0.0; Fy=0.0; Fz=0.0;
Mx=0.0; My=0.0; Mz=0.0;
idpt = mbs_data.xfidpt(ixF);
dxF = mbs_data.dpt(:,idpt);

%/*-- Begin of user code --*/
% 
% Use the 'mbs_get_F_sensor_id' function to get easily the force sensor
% indices, e.g. :
% F1 = mbs_get_F_sensor_id(MBS_info,'myFsensor_1');
% [F2,F3] = mbs_get_F_sensor_id(MBS_info,'myFsensor_2','myFsensor_3');
%

% Computes the total force to apply on the center of the robot
% when it impacts obstacles.

id_F_Robot_Ext_Force = mbs_get_F_sensor_id(MBS_info,'Robot_Ext_Force');

% robot properties
R_robot    = 0.1;   % robot body radius [m]

% contacts properties (spring with damper model)
k_contact  = 1.0e5; % stiffness [N/m]
kd_contact = 1.0e3; % damping   [N s/m]

switch(ixF)

    case id_F_Robot_Ext_Force % contact sensor
        
        % list with all obstacles
        circle_obstacles_list = MBS_user.circle_obstacles_list; 
        walls_list = MBS_user.walls_list; 
        
        % number of obstacles [-]
        [nb_circle_obstacles ~] = size(circle_obstacles_list);

        % robot position
        x_robot = PxF(1); % x position [m]
        y_robot = PxF(2); % y position [m]

        % robot velocity [m/s]
        xd_robot = VxF(1);
        yd_robot = VxF(2);
        
        for k=1:nb_circle_obstacles % loop on all the obstacles

            % obstacle position and radius
            x_obstacle = cell2mat(circle_obstacles_list(k,1)); % x position [m]
            y_obstacle = cell2mat(circle_obstacles_list(k,2)); % y position [m]
            R_obstacle = cell2mat(circle_obstacles_list(k,3)); % radius [m]
            
            % distances [m]
            diff_x = x_robot - x_obstacle;
            diff_y = y_robot - y_obstacle;
            
            % distance between their centers [m]
            dist = sqrt( diff_x^2 + diff_y^2 );
            
            % shortest distance between their borders [m] (negative if contact)
            dist_min_R = dist - R_robot - R_obstacle;
            
            % contact happening
            if dist_min_R < 0

                % derivative of 'dist_min_R' [m/s]
                contact_velocity = ( diff_x * xd_robot + diff_y * yd_robot ) / dist;
                
                % absolute angle of contact [rad]
                contact_angle = atan2(y_robot - y_obstacle , x_robot - x_obstacle);  
                
                % force to expel the body [N] (like a spring with damping)
                F_abs = -(k_contact * dist_min_R + kd_contact * contact_velocity);          
               
                % x and y components of the force [N]
                Fx = Fx + F_abs * cos(contact_angle);
                Fy = Fy + F_abs * sin(contact_angle);
            end
          
        end
        
        
        % 1: repel for x > ...
        dist_wall = walls_list{1} - R_robot - x_robot;

        if (dist_wall < 0.0)
            Fx = Fx + (k_contact * dist_wall - kd_contact * xd_robot);
        end

        % 2: repel for x < ...
        dist_wall = x_robot - walls_list{2} - R_robot;

        if (dist_wall < 0.0)
            Fx = Fx - (k_contact * dist_wall + kd_contact * xd_robot);
        end

        % 3: repel for y > ...
        dist_wall = walls_list{3} - R_robot - y_robot;

        if (dist_wall < 0.0)
            Fy = Fy + (k_contact * dist_wall - kd_contact * yd_robot);
        end

        % 4: repel for y < ...
        dist_wall = y_robot - walls_list{4} - R_robot;

        if (dist_wall < 0.0)
            Fy = Fy - (k_contact * dist_wall + kd_contact * yd_robot);
        end
        
    otherwise
end

%/*-- End of user code --*/

Swr = [Fx; Fy; Fz; Mx; My; Mz; dxF];

return
