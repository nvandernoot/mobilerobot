//---------------------------
//
// Nicolas Van der Noot
//
// Creation : 01/11/2013
// Last update : 06/07/2014
//
// Main loop of the controller file
//
//---------------------------

#include "controller_def.h"

void controller_loop(ControllerStruct *cvs)
{
	/*
	 * Wheel velocity references computed by 'compute_wheel_references' for the Simulink version.
	 * In the Standalone version, the user modifies these references with the keyboard or a joystick.
	 */
	#ifndef STANDALONE
	compute_wheel_references(cvs);
	#endif

	// P controller to get the motor commands
	proportional_controller(cvs);
}
